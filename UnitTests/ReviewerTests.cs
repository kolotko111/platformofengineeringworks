﻿using Domain;
using Domain.Models;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using UnitTests.Infrastructure;
using WebUI.Controllers.Reviewer;

namespace UnitTests
{
    class ReviewerTests
    {
        [TestCase("123-456-789")]
        [TestCase("456-789-123")]
        [TestCase("789-123-456")]
        public void DetailsTest(string id)
        {
            // Arrange
            var Users = new List<ApplicationUser>
            {
                new ApplicationUser { Id = Guid.NewGuid().ToString(),
                UserName = "student@wp.pl",
                Name = "student",
                Email = "student@wp.pl"},

                new ApplicationUser { Id = Guid.NewGuid().ToString(),
                UserName = "promoter@wp.pl",
                Name = "promoter",
                Email = "promoter@wp.pl"}
            }.AsQueryable();

            var AppForms = new List<ApplicationForm>
            {
                new ApplicationForm { Id = "123-456-789",
                AuthorName = "Author1",
                AuthorAddress = "AuthorAddress1",
                AuthorZipCode = "663-400",
                AuthorCity = "Ostrów Wielkopolski",
                Title = "Title1",
                School = "School1",
                TypeOfEngineeringWork = "TypeOfEngineeringWork1",
                CurrentAuthorId = Users.Where(q => q.Name == "student").First().Id,
                CurrentAuthor = Users.Where(q => q.Name == "student").First(),
                PromoterId = Users.Where(q => q.Name == "promoter").First().Id,
                Promoter = Users.Where(q => q.Name == "promoter").First()},

                new ApplicationForm { Id = "456-789-123",
                AuthorName = "Author2",
                AuthorAddress = "AuthorAddress2",
                AuthorZipCode = "663-400",
                AuthorCity = "Ostrów Wielkopolski",
                Title = "Title2",
                School = "School2",
                TypeOfEngineeringWork = "TypeOfEngineeringWork2",
                CurrentAuthorId = Users.Where(q => q.Name == "student").First().Id,
                CurrentAuthor = Users.Where(q => q.Name == "student").First(),
                PromoterId = Users.Where(q => q.Name == "promoter").First().Id,
                Promoter = Users.Where(q => q.Name == "promoter").First()},

                new ApplicationForm { Id = "789-123-456",
                AuthorName = "Author3",
                AuthorAddress = "AuthorAddress3",
                AuthorZipCode = "663-400",
                AuthorCity = "Ostrów Wielkopolski",
                Title = "Title3",
                School = "School3",
                TypeOfEngineeringWork = "TypeOfEngineeringWork3",
                CurrentAuthorId = Users.Where(q => q.Name == "student").First().Id,
                CurrentAuthor = Users.Where(q => q.Name == "student").First(),
                PromoterId = Users.Where(q => q.Name == "promoter").First().Id,
                Promoter = Users.Where(q => q.Name == "promoter").First()}
            }.AsQueryable();


            var mockSetForm = MockDbSet.GetMockDbSetAsync(AppForms);
            mockSetForm.Setup(m => m.Include(It.IsAny<String>())).Returns(mockSetForm.Object);

            var mockContext = new Mock<ApplicationDbContext>();
            mockContext.Setup(c => c.ApplicationForm).Returns(mockSetForm.Object);

            var service = new ReviewerController(mockContext.Object);

            //Act
            var applicationForm = service.Details(id).Result as ViewResult;
            var model = (ApplicationForm)applicationForm.ViewData.Model;

            //Assert
            Assert.AreEqual(AppForms.Where(q => q.Id == id).First(), model);
        }


        [TestCase(null, 400, "Application form id is empty")]
        [TestCase("123", 404, "Application form was not found")]
        public void DetailsStatusCodeTest(string input, int statusCode, string description)
        {
            // Arrange
            var Users = new List<ApplicationUser>
            {
                new ApplicationUser { Id = Guid.NewGuid().ToString(),
                UserName = "student@wp.pl",
                Name = "student",
                Email = "student@wp.pl"},

                new ApplicationUser { Id = Guid.NewGuid().ToString(),
                UserName = "promoter@wp.pl",
                Name = "promoter",
                Email = "promoter@wp.pl"}
            }.AsQueryable();

            var AppForms = new List<ApplicationForm>
            {
                new ApplicationForm { Id = "123-456-789",
                AuthorName = "Author1",
                AuthorAddress = "AuthorAddress1",
                AuthorZipCode = "663-400",
                AuthorCity = "Ostrów Wielkopolski",
                Title = "Title1",
                School = "School1",
                TypeOfEngineeringWork = "TypeOfEngineeringWork1",
                CurrentAuthorId = Users.Where(q => q.Name == "student").First().Id,
                CurrentAuthor = Users.Where(q => q.Name == "student").First(),
                PromoterId = Users.Where(q => q.Name == "promoter").First().Id,
                Promoter = Users.Where(q => q.Name == "promoter").First()},

                new ApplicationForm { Id = "456-789-123",
                AuthorName = "Author2",
                AuthorAddress = "AuthorAddress2",
                AuthorZipCode = "663-400",
                AuthorCity = "Ostrów Wielkopolski",
                Title = "Title2",
                School = "School2",
                TypeOfEngineeringWork = "TypeOfEngineeringWork2",
                CurrentAuthorId = Users.Where(q => q.Name == "student").First().Id,
                CurrentAuthor = Users.Where(q => q.Name == "student").First(),
                PromoterId = Users.Where(q => q.Name == "promoter").First().Id,
                Promoter = Users.Where(q => q.Name == "promoter").First()},

                new ApplicationForm { Id = "789-123-456",
                AuthorName = "Author3",
                AuthorAddress = "AuthorAddress3",
                AuthorZipCode = "663-400",
                AuthorCity = "Ostrów Wielkopolski",
                Title = "Title3",
                School = "School3",
                TypeOfEngineeringWork = "TypeOfEngineeringWork3",
                CurrentAuthorId = Users.Where(q => q.Name == "student").First().Id,
                CurrentAuthor = Users.Where(q => q.Name == "student").First(),
                PromoterId = Users.Where(q => q.Name == "promoter").First().Id,
                Promoter = Users.Where(q => q.Name == "promoter").First()}
            }.AsQueryable();


            var mockSetForm = MockDbSet.GetMockDbSetAsync(AppForms);
            mockSetForm.Setup(m => m.Include(It.IsAny<String>())).Returns(mockSetForm.Object);

            var mockContext = new Mock<ApplicationDbContext>();
            mockContext.Setup(c => c.ApplicationForm).Returns(mockSetForm.Object);

            var service = new ReviewerController(mockContext.Object);

            //Act
            var result = service.Details(input).Result as HttpStatusCodeResult;

            //Assert
            Assert.AreEqual(statusCode, result.StatusCode);
            Assert.AreEqual(description, result.StatusDescription);
        }


        [TestCase("123-456-789")]
        [TestCase("456-789-123")]
        [TestCase("789-123-456")]
        public void ViewMoreTest(string id)
        {
            // Arrange
            var Users = new List<ApplicationUser>
            {
                new ApplicationUser { Id = Guid.NewGuid().ToString(),
                UserName = "student@wp.pl",
                Name = "student",
                Email = "student@wp.pl"},

                new ApplicationUser { Id = Guid.NewGuid().ToString(),
                UserName = "promoter@wp.pl",
                Name = "promoter",
                Email = "promoter@wp.pl"}
            }.AsQueryable();

            var AppForms = new List<ApplicationForm>
            {
                new ApplicationForm { Id = "123-456-789",
                AuthorName = "Author1",
                AuthorAddress = "AuthorAddress1",
                AuthorZipCode = "663-400",
                AuthorCity = "Ostrów Wielkopolski",
                Title = "Title1",
                School = "School1",
                TypeOfEngineeringWork = "TypeOfEngineeringWork1",
                CurrentAuthorId = Users.Where(q => q.Name == "student").First().Id,
                CurrentAuthor = Users.Where(q => q.Name == "student").First(),
                PromoterId = Users.Where(q => q.Name == "promoter").First().Id,
                Promoter = Users.Where(q => q.Name == "promoter").First()},

                new ApplicationForm { Id = "456-789-123",
                AuthorName = "Author2",
                AuthorAddress = "AuthorAddress2",
                AuthorZipCode = "663-400",
                AuthorCity = "Ostrów Wielkopolski",
                Title = "Title2",
                School = "School2",
                TypeOfEngineeringWork = "TypeOfEngineeringWork2",
                CurrentAuthorId = Users.Where(q => q.Name == "student").First().Id,
                CurrentAuthor = Users.Where(q => q.Name == "student").First(),
                PromoterId = Users.Where(q => q.Name == "promoter").First().Id,
                Promoter = Users.Where(q => q.Name == "promoter").First()},

                new ApplicationForm { Id = "789-123-456",
                AuthorName = "Author3",
                AuthorAddress = "AuthorAddress3",
                AuthorZipCode = "663-400",
                AuthorCity = "Ostrów Wielkopolski",
                Title = "Title3",
                School = "School3",
                TypeOfEngineeringWork = "TypeOfEngineeringWork3",
                CurrentAuthorId = Users.Where(q => q.Name == "student").First().Id,
                CurrentAuthor = Users.Where(q => q.Name == "student").First(),
                PromoterId = Users.Where(q => q.Name == "promoter").First().Id,
                Promoter = Users.Where(q => q.Name == "promoter").First()}
            }.AsQueryable();


            var mockSetForm = MockDbSet.GetMockDbSetAsync(AppForms);
            mockSetForm.Setup(m => m.Include(It.IsAny<String>())).Returns(mockSetForm.Object);

            var mockContext = new Mock<ApplicationDbContext>();
            mockContext.Setup(c => c.ApplicationForm).Returns(mockSetForm.Object);

            var service = new ReviewerController(mockContext.Object);

            //Act
            var applicationForm = service.ViewMore(id).Result as ViewResult;
            var model = (ApplicationForm)applicationForm.ViewData.Model;

            //Assert
            Assert.AreEqual(AppForms.Where(q => q.Id == id).First(), model);
        }


        [TestCase("123-456-789")]
        [TestCase("456-789-123")]
        [TestCase("789-123-456")]
        public void CreateReviewGetTest(string id)
        {
            // Arrange
            var Users = new List<ApplicationUser>
            {
                new ApplicationUser { Id = Guid.NewGuid().ToString(),
                UserName = "student@wp.pl",
                Name = "student",
                Email = "student@wp.pl"},

                new ApplicationUser { Id = Guid.NewGuid().ToString(),
                UserName = "promoter@wp.pl",
                Name = "promoter",
                Email = "promoter@wp.pl"}
            }.AsQueryable();

            var AppForms = new List<ApplicationForm>
            {
                new ApplicationForm { Id = "123-456-789",
                AuthorName = "Author1",
                AuthorAddress = "AuthorAddress1",
                AuthorZipCode = "663-400",
                AuthorCity = "Ostrów Wielkopolski",
                Title = "Title1",
                School = "School1",
                TypeOfEngineeringWork = "TypeOfEngineeringWork1",
                CurrentAuthorId = Users.Where(q => q.Name == "student").First().Id,
                CurrentAuthor = Users.Where(q => q.Name == "student").First(),
                PromoterId = Users.Where(q => q.Name == "promoter").First().Id,
                Promoter = Users.Where(q => q.Name == "promoter").First()},

                new ApplicationForm { Id = "456-789-123",
                AuthorName = "Author2",
                AuthorAddress = "AuthorAddress2",
                AuthorZipCode = "663-400",
                AuthorCity = "Ostrów Wielkopolski",
                Title = "Title2",
                School = "School2",
                TypeOfEngineeringWork = "TypeOfEngineeringWork2",
                CurrentAuthorId = Users.Where(q => q.Name == "student").First().Id,
                CurrentAuthor = Users.Where(q => q.Name == "student").First(),
                PromoterId = Users.Where(q => q.Name == "promoter").First().Id,
                Promoter = Users.Where(q => q.Name == "promoter").First()},

                new ApplicationForm { Id = "789-123-456",
                AuthorName = "Author3",
                AuthorAddress = "AuthorAddress3",
                AuthorZipCode = "663-400",
                AuthorCity = "Ostrów Wielkopolski",
                Title = "Title3",
                School = "School3",
                TypeOfEngineeringWork = "TypeOfEngineeringWork3",
                CurrentAuthorId = Users.Where(q => q.Name == "student").First().Id,
                CurrentAuthor = Users.Where(q => q.Name == "student").First(),
                PromoterId = Users.Where(q => q.Name == "promoter").First().Id,
                Promoter = Users.Where(q => q.Name == "promoter").First()}
            }.AsQueryable();


            var mockSetForm = MockDbSet.GetMockDbSetAsync(AppForms);
            mockSetForm.Setup(m => m.Include(It.IsAny<String>())).Returns(mockSetForm.Object);

            var mockContext = new Mock<ApplicationDbContext>();
            mockContext.Setup(c => c.ApplicationForm).Returns(mockSetForm.Object);

            var service = new ReviewerController(mockContext.Object);

            //Act
            var resoult = service.CreateReview(id).Result as ViewResult;
            var model = (Review)resoult.ViewData.Model;
            var viewBag = resoult.ViewBag.Numbers;

            //Assert
            Assert.AreEqual(id, model.CurrentApplicationFormId);
            Assert.IsNotNull(viewBag);
        }
    }
}

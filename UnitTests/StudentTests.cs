﻿using Domain;
using Domain.Models;
using Microsoft.AspNet.Identity;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using UnitTests.Infrastructure;
using WebUI.Controllers;

namespace UnitTests
{
    class StudentTests
    {
        [TestCase("123-456-789")]
        [TestCase("456-789-123")]
        [TestCase("789-123-456")]
        public void DetailsTest(string id)
        {
            // Arrange
            var Users = new List<ApplicationUser>
            {
                new ApplicationUser { Id = Guid.NewGuid().ToString(),
                UserName = "student@wp.pl",
                Name = "student",
                Email = "student@wp.pl"},

                new ApplicationUser { Id = Guid.NewGuid().ToString(),
                UserName = "promoter@wp.pl",
                Name = "promoter",
                Email = "promoter@wp.pl"}
            }.AsQueryable();

            var AppForms = new List<ApplicationForm>
            {
                new ApplicationForm { Id = "123-456-789",
                AuthorName = "Author1",
                AuthorAddress = "AuthorAddress1",
                AuthorZipCode = "663-400",
                AuthorCity = "Ostrów Wielkopolski",
                Title = "Title1",
                School = "School1",
                TypeOfEngineeringWork = "TypeOfEngineeringWork1",
                CurrentAuthorId = Users.Where(q => q.Name == "student").First().Id,
                CurrentAuthor = Users.Where(q => q.Name == "student").First(),
                PromoterId = Users.Where(q => q.Name == "promoter").First().Id,
                Promoter = Users.Where(q => q.Name == "promoter").First()},

                new ApplicationForm { Id = "456-789-123",
                AuthorName = "Author2",
                AuthorAddress = "AuthorAddress2",
                AuthorZipCode = "663-400",
                AuthorCity = "Ostrów Wielkopolski",
                Title = "Title2",
                School = "School2",
                TypeOfEngineeringWork = "TypeOfEngineeringWork2",
                CurrentAuthorId = Users.Where(q => q.Name == "student").First().Id,
                CurrentAuthor = Users.Where(q => q.Name == "student").First(),
                PromoterId = Users.Where(q => q.Name == "promoter").First().Id,
                Promoter = Users.Where(q => q.Name == "promoter").First()},

                new ApplicationForm { Id = "789-123-456",
                AuthorName = "Author3",
                AuthorAddress = "AuthorAddress3",
                AuthorZipCode = "663-400",
                AuthorCity = "Ostrów Wielkopolski",
                Title = "Title3",
                School = "School3",
                TypeOfEngineeringWork = "TypeOfEngineeringWork3",
                CurrentAuthorId = Users.Where(q => q.Name == "student").First().Id,
                CurrentAuthor = Users.Where(q => q.Name == "student").First(),
                PromoterId = Users.Where(q => q.Name == "promoter").First().Id,
                Promoter = Users.Where(q => q.Name == "promoter").First()}
            }.AsQueryable();


            var mockSetForm = MockDbSet.GetMockDbSetAsync(AppForms);
            mockSetForm.Setup(m => m.Include(It.IsAny<String>())).Returns(mockSetForm.Object);

            var mockContext = new Mock<ApplicationDbContext>();
            mockContext.Setup(c => c.ApplicationForm).Returns(mockSetForm.Object);

            var service = new StudentController(mockContext.Object);

            //Act
            var applicationForm = service.Details(id).Result as ViewResult;
            //var xD = service.Details(id).Result is HttpNotFoundResult;
            
            

            var model = (ApplicationForm)applicationForm.ViewData.Model;

            //Assert
            Assert.AreEqual(AppForms.Where(q => q.Id == id).First(), model);
        }

        [TestCase(null, 400, "Application form id is empty")]
        [TestCase("123", 404, "Application form was not found")]
        public void DetailsStatusCodeTest(string input, int statusCode, string description)
        {
            // Arrange
            var Users = new List<ApplicationUser>
            {
                new ApplicationUser { Id = Guid.NewGuid().ToString(),
                UserName = "student@wp.pl",
                Name = "student",
                Email = "student@wp.pl"},

                new ApplicationUser { Id = Guid.NewGuid().ToString(),
                UserName = "promoter@wp.pl",
                Name = "promoter",
                Email = "promoter@wp.pl"}
            }.AsQueryable();

            var AppForms = new List<ApplicationForm>
            {
                new ApplicationForm { Id = "123-456-789",
                AuthorName = "Author1",
                AuthorAddress = "AuthorAddress1",
                AuthorZipCode = "663-400",
                AuthorCity = "Ostrów Wielkopolski",
                Title = "Title1",
                School = "School1",
                TypeOfEngineeringWork = "TypeOfEngineeringWork1",
                CurrentAuthorId = Users.Where(q => q.Name == "student").First().Id,
                CurrentAuthor = Users.Where(q => q.Name == "student").First(),
                PromoterId = Users.Where(q => q.Name == "promoter").First().Id,
                Promoter = Users.Where(q => q.Name == "promoter").First()},

                new ApplicationForm { Id = "456-789-123",
                AuthorName = "Author2",
                AuthorAddress = "AuthorAddress2",
                AuthorZipCode = "663-400",
                AuthorCity = "Ostrów Wielkopolski",
                Title = "Title2",
                School = "School2",
                TypeOfEngineeringWork = "TypeOfEngineeringWork2",
                CurrentAuthorId = Users.Where(q => q.Name == "student").First().Id,
                CurrentAuthor = Users.Where(q => q.Name == "student").First(),
                PromoterId = Users.Where(q => q.Name == "promoter").First().Id,
                Promoter = Users.Where(q => q.Name == "promoter").First()},

                new ApplicationForm { Id = "789-123-456",
                AuthorName = "Author3",
                AuthorAddress = "AuthorAddress3",
                AuthorZipCode = "663-400",
                AuthorCity = "Ostrów Wielkopolski",
                Title = "Title3",
                School = "School3",
                TypeOfEngineeringWork = "TypeOfEngineeringWork3",
                CurrentAuthorId = Users.Where(q => q.Name == "student").First().Id,
                CurrentAuthor = Users.Where(q => q.Name == "student").First(),
                PromoterId = Users.Where(q => q.Name == "promoter").First().Id,
                Promoter = Users.Where(q => q.Name == "promoter").First()}
            }.AsQueryable();


            var mockSetForm = MockDbSet.GetMockDbSetAsync(AppForms);
            mockSetForm.Setup(m => m.Include(It.IsAny<String>())).Returns(mockSetForm.Object);

            var mockContext = new Mock<ApplicationDbContext>();
            mockContext.Setup(c => c.ApplicationForm).Returns(mockSetForm.Object);

            var service = new StudentController(mockContext.Object);

            //Act
            var result = service.Details(input).Result as HttpStatusCodeResult;

            //Assert
            Assert.AreEqual(statusCode, result.StatusCode);
            Assert.AreEqual(description, result.StatusDescription);
        }


        [TestCase("123-456-789")]
        [TestCase("456-789-123")]
        [TestCase("789-123-456")]
        public void DeletePostTest(string id)
        {
            // Arrange
            var Users = new List<ApplicationUser>
            {
                new ApplicationUser { Id = Guid.NewGuid().ToString(),
                UserName = "student@wp.pl",
                Name = "student",
                Email = "student@wp.pl"},

                new ApplicationUser { Id = Guid.NewGuid().ToString(),
                UserName = "promoter@wp.pl",
                Name = "promoter",
                Email = "promoter@wp.pl"}
            }.AsQueryable();

            var AppForms = new List<ApplicationForm>
            {
                new ApplicationForm { Id = "123-456-789",
                AuthorName = "Author1",
                AuthorAddress = "AuthorAddress1",
                AuthorZipCode = "663-400",
                AuthorCity = "Ostrów Wielkopolski",
                Title = "Title1",
                School = "School1",
                TypeOfEngineeringWork = "TypeOfEngineeringWork1",
                CurrentAuthorId = Users.Where(q => q.Name == "student").First().Id,
                CurrentAuthor = Users.Where(q => q.Name == "student").First(),
                PromoterId = Users.Where(q => q.Name == "promoter").First().Id,
                Promoter = Users.Where(q => q.Name == "promoter").First()},

                new ApplicationForm { Id = "456-789-123",
                AuthorName = "Author2",
                AuthorAddress = "AuthorAddress2",
                AuthorZipCode = "663-400",
                AuthorCity = "Ostrów Wielkopolski",
                Title = "Title2",
                School = "School2",
                TypeOfEngineeringWork = "TypeOfEngineeringWork2",
                CurrentAuthorId = Users.Where(q => q.Name == "student").First().Id,
                CurrentAuthor = Users.Where(q => q.Name == "student").First(),
                PromoterId = Users.Where(q => q.Name == "promoter").First().Id,
                Promoter = Users.Where(q => q.Name == "promoter").First()},

                new ApplicationForm { Id = "789-123-456",
                AuthorName = "Author3",
                AuthorAddress = "AuthorAddress3",
                AuthorZipCode = "663-400",
                AuthorCity = "Ostrów Wielkopolski",
                Title = "Title3",
                School = "School3",
                TypeOfEngineeringWork = "TypeOfEngineeringWork3",
                CurrentAuthorId = Users.Where(q => q.Name == "student").First().Id,
                CurrentAuthor = Users.Where(q => q.Name == "student").First(),
                PromoterId = Users.Where(q => q.Name == "promoter").First().Id,
                Promoter = Users.Where(q => q.Name == "promoter").First()}
            };


            var mockSetForm = MockDbSet.GetMockDbSetAsync(AppForms);
            mockSetForm.Setup(m => m.Include(It.IsAny<String>())).Returns(mockSetForm.Object);

            var mockContext = new Mock<ApplicationDbContext>();
            mockContext.Setup(c => c.ApplicationForm).Returns(mockSetForm.Object);
            mockContext.Setup(m => m.ApplicationForm.Remove(It.IsAny<ApplicationForm>())).Callback<ApplicationForm>((entity) => AppForms.Remove(entity));

            var service = new StudentController(mockContext.Object);

            ApplicationForm selectedForm = AppForms.Where(q => q.Id == id).First();
            //Act
            var result = (RedirectToRouteResult)service.Delete(selectedForm).Result;

            Assert.AreEqual("Index", result.RouteValues["action"]);
            Assert.AreEqual("Student", result.RouteValues["controller"]);

            ApplicationForm removedRecord = mockContext.Object.ApplicationForm.Where(q => q.Id == id).FirstOrDefault();
            Assert.IsNull(removedRecord);
        }


        [TestCase("123-456-789")]
        [TestCase("456-789-123")]
        public void DisplayReviewTest(string id)
        {
            // Arrange
            var Reviews = new List<Review>
            {
                new Review {Id = "123-456-789",
                SubjectOfWork = 3,
                UsefulnessForPractice = 3,
                OwnResearch = 3,
                Originality = 3,
                UsedLiterature = 3,
                LanguageAndFormOfWork =3},

                new Review {Id = "456-789-123",
                SubjectOfWork = 5,
                UsefulnessForPractice = 5,
                OwnResearch = 5,
                Originality = 5,
                UsedLiterature = 5,
                LanguageAndFormOfWork =5}
            };

            var mockSetReview = MockDbSet.GetMockDbSetAsync(Reviews);

            var mockContext = new Mock<ApplicationDbContext>();
            mockContext.Setup(c => c.Review).Returns(mockSetReview.Object);

            var service = new StudentController(mockContext.Object);

            //Act
            var returnedReview = service.DisplayReview(id).Result as ViewResult;
            var model = (Review)returnedReview.ViewData.Model;

            //Assert
            Assert.AreEqual(Reviews.Where(q => q.Id == id).First(), model);
        }


    }
}

﻿using Domain;
using Domain.Enum;
using Domain.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using NLog;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using WebUI.ViewModels;

namespace WebUI.Controllers.Promoter
{
    public class PromoterController : Controller
    {
        private ApplicationDbContext _context;
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public PromoterController()
        {

        }

        public PromoterController(ApplicationDbContext context)
        {
            Context = context;
        }

        public ApplicationDbContext Context
        {
            get
            {
                return _context ?? HttpContext.GetOwinContext().GetUserManager<ApplicationDbContext>();
            }
            private set
            {
                _context = value;
            }
        }

        [Authorize(Roles = "Promoter")]
        public ActionResult Index()
        {
            return View();
        }

        [Authorize(Roles = "Promoter")]
        public async Task<ActionResult> ConfirmedForms(string email = null, string titleOfEngineeringWork = null)
        {
            var currentUserId = User.Identity.GetUserId();
            if (string.IsNullOrEmpty(currentUserId))
            {
                logger.Warn("ConfirmedForms post: Invalid current user id");
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Invalid current user id");
            }

            var currentUser = await Context.Users.Include(q => q.ApplicationFormForPromoter).Where(w => w.Id == currentUserId).FirstOrDefaultAsync();

            var confirmedFormsForUser = currentUser.ApplicationFormForPromoter.Where(e => e.PromotorConfirmedForm == true).AsQueryable();
            if (!string.IsNullOrEmpty(email))
            {
                confirmedFormsForUser = currentUser.ApplicationFormForPromoter.Where(e => e.CurrentAuthor.Email.Contains(email)).AsQueryable();
            }

            if (!string.IsNullOrEmpty(titleOfEngineeringWork))
            {
                confirmedFormsForUser = confirmedFormsForUser.Where(q => q.Title.Contains(titleOfEngineeringWork));
            }

            return View(confirmedFormsForUser.AsEnumerable());
        }

        [Authorize(Roles = "Promoter")]
        public async Task<ActionResult> UnconfirmedForms(string email = null, string titleOfEngineeringWork = null)
        {
            //get current promoter id
            var currentUserId = User.Identity.GetUserId();
            if (string.IsNullOrEmpty(currentUserId))
            {
                logger.Warn("UnconfirmedForms post: Invalid current user id");
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Invalid current user id");
            }
            //get all confirmed forms for promotor 
            var currentUser = await Context.Users.Include(q => q.ApplicationFormForPromoter).Where(w => w.Id == currentUserId).FirstOrDefaultAsync();

            var unconfirmedFormsForUser = currentUser.ApplicationFormForPromoter.Where(e => e.PromotorConfirmedForm == false).AsQueryable();
            if (!string.IsNullOrEmpty(email))
            {
                unconfirmedFormsForUser = currentUser.ApplicationFormForPromoter.Where(e => e.CurrentAuthor.Email.Contains(email)).AsQueryable();
            }

            if (!string.IsNullOrEmpty(titleOfEngineeringWork))
            {
                unconfirmedFormsForUser = currentUser.ApplicationFormForPromoter.Where(e => e.Title.Contains(titleOfEngineeringWork)).AsQueryable();
            }
            
            return View(unconfirmedFormsForUser.AsEnumerable());
        }

        [Authorize(Roles = "Promoter")]
        public async Task<ActionResult> ConfirmForms(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                logger.Warn("Application form id was empty");
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Invalid id");
            }

            var currentForm = await Context.ApplicationForm.Include(q => q.CurrentAuthor).Include(q => q.Promoter).Include(q => q.PdfFile).Where(w => w.Id == id).FirstOrDefaultAsync();
            if (currentForm == null)
            {
                logger.Warn("Application form was not found in db");
                return new HttpNotFoundResult("Application form not found");
            }

            return View(currentForm);
        }

        [HttpPost]
        [Authorize(Roles = "Promoter")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ConfirmForms(ApplicationForm model)
        {
            var form = await Context.ApplicationForm.Where(q => q.Id == model.Id).FirstOrDefaultAsync();
            if (form == null)
            {
                logger.Warn("ConfirmForms post: Application form was not found");
                return new HttpNotFoundResult("Application form not found");
            }
            form.PromotorConfirmedForm = true;

            try
            {
                await Context.SaveChangesAsync();
                logger.Info("ConfirmForms post: Application form confirmed promoter");
            }
            catch (Exception ex)
            {
                logger.Error(ex, "ConfirmForms post: Application Form exception");
                return new HttpStatusCodeResult(500);
            }
            

            return RedirectToAction("Index", "Promoter");
        }

        [Authorize(Roles = "Promoter")]
        public async Task<ActionResult> Details(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                logger.Warn("Details get: Application form id is empty");
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Application form id is empty");
            }

            var applicationForm = await Context.ApplicationForm.Include(s => s.CurrentAuthor).Include(s => s.Promoter).Include(s => s.PdfFile).Include(s => s.Reviewers).Include(s => s.Reviews).Where(q => q.Id == id).FirstOrDefaultAsync();
            if (applicationForm == null)
            {
                logger.Warn("Details get: Application form was not found");
                return new HttpNotFoundResult("Application form was not found");
            }

            return View(applicationForm);
        }

        [Authorize(Roles = "Promoter")]
        public async Task<ActionResult> DisplayReview(string reviewsId)
        {
            if (string.IsNullOrEmpty(reviewsId))
            {
                logger.Warn("DisplayReview get: Review id is empty");
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Review id is empty");
            }

            var review = await Context.Review.Where(q => q.Id == reviewsId).FirstOrDefaultAsync();
            if (review == null)
            {
                logger.Warn("DisplayReview get: Review was not found");
                return new HttpNotFoundResult("Review was not found");
            }

            return View(review);
        }

        [Authorize(Roles = "Promoter")]
        public ActionResult ReturnFile(string applicationFormId, KindOfEngineeringWorks kindOfEngineeringWorks)
        {
            if (string.IsNullOrEmpty(applicationFormId))
            {
                logger.Warn("ReturnFile get: Application form id is empty");
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Application form id is empty");
            }

            var pdfFile = Context.PdfModel.Where(q => q.CurrentApplicationFormId == applicationFormId && q.KindOfEngineeringWorks == kindOfEngineeringWorks).FirstOrDefault();
            if (pdfFile == null)
            {
                logger.Warn("ReturnFile get: Pdf file was not found");
                return new HttpNotFoundResult("Pdf file was not found");
            }

            return File(pdfFile.File, pdfFile.ContentType, pdfFile.Name);
        }

        [Authorize(Roles = "Promoter")]
        public ActionResult Create()
        {
            var currentUserId = User.Identity.GetUserId();
            if (string.IsNullOrEmpty(currentUserId))
            {
                logger.Warn("Create get: Invalid current user id");
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Invalid current user id");
            }

            ApplicationFormViewModels viewModel = new ApplicationFormViewModels();
            ApplicationForm model = new ApplicationForm();
            model.PromoterId = currentUserId;
            viewModel.ApplicationForm = model;
            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Promoter")]
        public async Task<ActionResult> Create(ApplicationFormViewModels model)
        {
            if (ModelState.IsValid)
            {
                var currentUserId = User.Identity.GetUserId();
                if (string.IsNullOrEmpty(currentUserId))
                {
                    logger.Warn("Create post: Invalid current user id");
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Invalid current user id");
                }

                var currentUser = await Context.Users.Where(q => q.Id == currentUserId).FirstOrDefaultAsync();
                if (currentUser == null)
                {
                    logger.Warn("Create post: Current user was not found");
                    return new HttpNotFoundResult("Current user was not found");
                }

                //create Application form
                ApplicationForm applicationForm = new ApplicationForm();
                applicationForm.Id = Guid.NewGuid().ToString();
                applicationForm.AuthorName = model.ApplicationForm.AuthorName;
                applicationForm.AuthorAddress = model.ApplicationForm.AuthorAddress;
                applicationForm.AuthorZipCode = model.ApplicationForm.AuthorZipCode;
                applicationForm.AuthorCity = model.ApplicationForm.AuthorCity;
                applicationForm.Title = model.ApplicationForm.Title;
                applicationForm.School = model.ApplicationForm.School;
                applicationForm.TypeOfEngineeringWork = model.ApplicationForm.TypeOfEngineeringWork;
                applicationForm.DateOfCreateForm = DateTime.Now;
                applicationForm.CurrentAuthor = currentUser;

                var promoterId = model.ApplicationForm.PromoterId;
                if (string.IsNullOrEmpty(promoterId))
                {
                    logger.Warn("Create post: Promoter id is empty");
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Promoter id is empty");
                }
                applicationForm.PromoterId = promoterId;

                var promoter = await Context.Users.Where(q => q.Id == model.ApplicationForm.PromoterId).FirstOrDefaultAsync();
                if (promoter == null)
                {
                    logger.Warn("Create post: Promoter was not found");
                    return new HttpNotFoundResult("Promoter was not found");
                }
                applicationForm.Promoter = promoter;

                //create pdf file 
                PdfModel EngineeringWork = new PdfModel();
                PdfModel SummaryPl = new PdfModel();
                PdfModel SummaryEn = new PdfModel();

                //strim for Pdf file
                if (model.EngineeringWork != null)
                {
                    //copy pdf to memory 
                    MemoryStream target = new MemoryStream();
                    model.EngineeringWork.InputStream.CopyTo(target);

                    //generate id for pdf 
                    EngineeringWork.Id = Guid.NewGuid().ToString();
                    EngineeringWork.Name = model.EngineeringWork.FileName;
                    EngineeringWork.ContentType = model.EngineeringWork.ContentType;
                    EngineeringWork.File = target.ToArray();
                    EngineeringWork.CurrentApplicationFormId = applicationForm.Id;
                    EngineeringWork.CurrentApplicationForm = applicationForm;
                    EngineeringWork.KindOfEngineeringWorks = KindOfEngineeringWorks.EngineeringWork;

                    Context.PdfModel.Add(EngineeringWork);

                    if (model.SummaryPl != null)
                    {
                        //copy pdf to memory 
                        MemoryStream SummaryPlTarget = new MemoryStream();
                        model.SummaryPl.InputStream.CopyTo(SummaryPlTarget);

                        SummaryPl.Id = Guid.NewGuid().ToString();
                        SummaryPl.Name = model.SummaryPl.FileName;
                        SummaryPl.ContentType = model.SummaryPl.ContentType;
                        SummaryPl.File = SummaryPlTarget.ToArray();
                        SummaryPl.CurrentApplicationFormId = applicationForm.Id;
                        SummaryPl.CurrentApplicationForm = applicationForm;
                        SummaryPl.KindOfEngineeringWorks = KindOfEngineeringWorks.SummaryPl;

                        Context.PdfModel.Add(SummaryPl);
                    }

                    if (model.SummaryEn != null)
                    {
                        //copy pdf to memory 
                        MemoryStream SummaryEnTarget = new MemoryStream();
                        model.SummaryEn.InputStream.CopyTo(SummaryEnTarget);

                        SummaryEn.Id = Guid.NewGuid().ToString();
                        SummaryEn.Name = model.SummaryEn.FileName;
                        SummaryEn.ContentType = model.SummaryEn.ContentType;
                        SummaryEn.File = SummaryEnTarget.ToArray();
                        SummaryEn.CurrentApplicationFormId = applicationForm.Id;
                        SummaryEn.CurrentApplicationForm = applicationForm;
                        SummaryEn.KindOfEngineeringWorks = KindOfEngineeringWorks.SummaryEn;

                        Context.PdfModel.Add(SummaryEn);
                    }
                }

                try
                {
                    Context.ApplicationForm.Add(applicationForm);
                    await Context.SaveChangesAsync();
                    logger.Info("Create post: Application form correctly added");
                }
                catch (Exception ex)
                {
                    logger.Error(ex, "Create post: Application form exception");
                    return new HttpStatusCodeResult(500);
                }
                
                return RedirectToAction("Index");
            }
            else
            {
                return View();
            }
        }

        [Authorize(Roles = "Promoter")]
        public async Task<ActionResult> Edit(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                logger.Warn("Edit get: Application form id is empty");
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Application form id is empty");
            }

            var applicationForm = await Context.ApplicationForm.Include(s => s.CurrentAuthor).Include(s => s.Promoter).Include(s => s.PdfFile).Include(s => s.Reviewers).Include(s => s.Reviews).Where(q => q.Id == id).FirstOrDefaultAsync();
            if (applicationForm == null)
            {
                logger.Warn("Edit get: Application form was not found");
                return new HttpNotFoundResult("Application form was not found");
            }

            ApplicationFormViewModels viewModels = new ApplicationFormViewModels();
            viewModels.ApplicationForm = applicationForm;

            return View(viewModels);
        }

        [HttpPost]
        [Authorize(Roles = "Promoter")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(ApplicationFormViewModels model)
        {
                if (ModelState.IsValid)
                {
                var applicationForm = await Context.ApplicationForm.Include(s => s.CurrentAuthor).Include(s => s.Promoter).Include(s => s.PdfFile).Include(s => s.Reviewers).Include(s => s.Reviews).Where(q => q.Id == model.ApplicationForm.Id).FirstOrDefaultAsync();
                if (applicationForm == null)
                {
                    logger.Warn("Edit post: Application form was not found");
                    return new HttpNotFoundResult("Application form was not found");
                }

                var pdfFile = await Context.PdfModel.Where(q => q.CurrentApplicationFormId == model.ApplicationForm.Id).ToListAsync();

                applicationForm.AuthorAddress = model.ApplicationForm.AuthorAddress;
                applicationForm.AuthorZipCode = model.ApplicationForm.AuthorZipCode;
                applicationForm.AuthorCity = model.ApplicationForm.AuthorCity;
                applicationForm.Title = model.ApplicationForm.Title;
                applicationForm.School = model.ApplicationForm.School;
                applicationForm.TypeOfEngineeringWork = model.ApplicationForm.TypeOfEngineeringWork;

                var promoterId = model.ApplicationForm.PromoterId;
                if (string.IsNullOrEmpty(promoterId))
                {
                    logger.Warn("Edit post: Promoter id is empty");
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Promoter id is empty");
                }
                applicationForm.PromoterId = promoterId;

                var promoter = await Context.Users.Where(q => q.Id == model.ApplicationForm.PromoterId).FirstOrDefaultAsync();
                if (promoter == null)
                {
                    logger.Warn("Edit post: Promoter was not found");
                    return new HttpNotFoundResult("Promoter was not found");
                }
                applicationForm.Promoter = promoter;

                //Application allways have Engineering Work
                if (model.EngineeringWork != null)
                {
                    var EngineeringWork = pdfFile.Where(q => q.KindOfEngineeringWorks == KindOfEngineeringWorks.EngineeringWork).FirstOrDefault();
                    //copy pdf to memory 
                    MemoryStream target = new MemoryStream();
                    model.EngineeringWork.InputStream.CopyTo(target);

                    EngineeringWork.Name = model.EngineeringWork.FileName;
                    EngineeringWork.ContentType = model.EngineeringWork.ContentType;
                    EngineeringWork.File = target.ToArray();
                    EngineeringWork.CurrentApplicationFormId = applicationForm.Id;
                    EngineeringWork.CurrentApplicationForm = applicationForm;
                    EngineeringWork.KindOfEngineeringWorks = KindOfEngineeringWorks.EngineeringWork;
                }

                if (model.SummaryPl != null)
                {
                    var SummaryPl = pdfFile.Where(q => q.KindOfEngineeringWorks == KindOfEngineeringWorks.SummaryPl).FirstOrDefault();

                    if (SummaryPl == null)
                    {
                        SummaryPl = new PdfModel();
                        SummaryPl.Id = Guid.NewGuid().ToString();
                        Context.PdfModel.Add(SummaryPl);
                    }
                    MemoryStream SummaryPlTarget = new MemoryStream();
                    model.SummaryPl.InputStream.CopyTo(SummaryPlTarget);

                    SummaryPl.Name = model.SummaryPl.FileName;
                    SummaryPl.ContentType = model.SummaryPl.ContentType;
                    SummaryPl.File = SummaryPlTarget.ToArray();
                    SummaryPl.CurrentApplicationFormId = applicationForm.Id;
                    SummaryPl.CurrentApplicationForm = applicationForm;
                    SummaryPl.KindOfEngineeringWorks = KindOfEngineeringWorks.SummaryPl;

                }

                if (model.SummaryEn != null)
                {
                    var SummaryEn = pdfFile.Where(q => q.KindOfEngineeringWorks == KindOfEngineeringWorks.SummaryPl).FirstOrDefault();

                    if (SummaryEn == null)
                    {
                        SummaryEn = new PdfModel();
                        SummaryEn.Id = Guid.NewGuid().ToString();
                        Context.PdfModel.Add(SummaryEn);
                    }

                    MemoryStream SummaryEnTarget = new MemoryStream();
                    model.SummaryEn.InputStream.CopyTo(SummaryEnTarget);

                    SummaryEn.Name = model.SummaryEn.FileName;
                    SummaryEn.ContentType = model.SummaryEn.ContentType;
                    SummaryEn.File = SummaryEnTarget.ToArray();
                    SummaryEn.CurrentApplicationFormId = applicationForm.Id;
                    SummaryEn.CurrentApplicationForm = applicationForm;
                    SummaryEn.KindOfEngineeringWorks = KindOfEngineeringWorks.SummaryEn;

                }

                try
                {
                    await Context.SaveChangesAsync();
                    logger.Info("Edit post: Application form correctly edited");
                }
                catch (Exception ex)
                {
                    logger.Error(ex, "Edit post: Application form exception");
                    return new HttpStatusCodeResult(500);
                }
                

                return RedirectToAction("Index");
            }
            else
            {
                if (string.IsNullOrEmpty(model.ApplicationForm.Id))
                {
                    logger.Warn("Edit post: Application form id is empty");
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Application form id is empty");
                }

                var applicationForm = await Context.ApplicationForm.Include(s => s.CurrentAuthor).Include(s => s.Promoter).Include(s => s.PdfFile).Include(s => s.Reviewers).Include(s => s.Reviews).Where(q => q.Id == model.ApplicationForm.Id).FirstOrDefaultAsync();
                if (applicationForm == null)
                {
                    logger.Warn("Edit post: Application form was not found");
                    return new HttpNotFoundResult("Application form was not found");
                }

                List<SelectListItem> listOfPromoters = new List<SelectListItem>();

                ApplicationFormViewModels viewModels = new ApplicationFormViewModels();
                viewModels.ApplicationForm = applicationForm;

                return View(viewModels);
            }
        }

        [Authorize(Roles = "Promoter")]
        public async Task<ActionResult> Delete(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                logger.Warn("Delete get: Application form id is empty");
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Application form id is empty");
            }

            var applicationForm = await Context.ApplicationForm.Include(s => s.CurrentAuthor).Include(s => s.Promoter).Include(s => s.PdfFile).Include(s => s.Reviewers).Include(s => s.Reviews).Where(q => q.Id == id).FirstOrDefaultAsync();
            if (applicationForm == null)
            {
                logger.Warn("Delete get: Application form was not found");
                return new HttpNotFoundResult("Application form was not found");
            }

            return View(applicationForm);
        }

        [HttpPost]
        [Authorize(Roles = "Promoter")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Delete(ApplicationForm model)
        {
            var applicationForm = await Context.ApplicationForm.Include(s => s.CurrentAuthor).Include(s => s.Promoter).Include(s => s.PdfFile).Include(s => s.Reviewers).Include(s => s.Reviews).Where(q => q.Id == model.Id).FirstOrDefaultAsync();
            if (applicationForm == null)
            {
                logger.Warn("Delete post: Application form was not found");
                return new HttpNotFoundResult("Application form was not found");
            }

            try
            {
                Context.ApplicationForm.Remove(applicationForm);
                await Context.SaveChangesAsync();
                logger.Info("Delete post: Application form correctly removed");
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Delete post: Application form exception");
                return new HttpStatusCodeResult(500);
            }
            
            return RedirectToAction("Index", "Promoter");
        }
    }
}
﻿using Domain;
using Domain.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Moq;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using UnitTests.Infrastructure;
using WebUI.Controllers.Admin;
using WebUI.Controllers.Promoter;
using WebUI.Controllers.Reviewer;
using WebUI.ViewModels;

namespace WebUI.Controllers
{
    public class HomeController : Controller
    {
        private ApplicationDbContext _context;
        private ApplicationUserManager _userManager;

        public HomeController()
        {
        }

        public HomeController(ApplicationUserManager userManager, ApplicationDbContext context)
        {
            UserManager = userManager;
            Context = context;
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public ApplicationDbContext Context
        {
            get
            {
                return _context ?? HttpContext.GetOwinContext().GetUserManager<ApplicationDbContext>();
            }
            private set
            {
                _context = value;
            }
        }

        public async Task<ActionResult> Index()
        {

            // Arrange
            var Users = new List<ApplicationUser>
            {
                new ApplicationUser { Id = Guid.NewGuid().ToString(),
                UserName = "student@wp.pl",
                Name = "student",
                Email = "student@wp.pl"},

                new ApplicationUser { Id = Guid.NewGuid().ToString(),
                UserName = "promoter@wp.pl",
                Name = "promoter",
                Email = "promoter@wp.pl"}
            }.AsQueryable();

            var AppForms = new List<ApplicationForm>
            {
                new ApplicationForm { Id = "123-456-789",
                AuthorName = "Author1",
                AuthorAddress = "AuthorAddress1",
                AuthorZipCode = "663-400",
                AuthorCity = "Ostrów Wielkopolski",
                Title = "Title1",
                School = "School1",
                TypeOfEngineeringWork = "TypeOfEngineeringWork1",
                CurrentAuthorId = Users.Where(q => q.Name == "student").First().Id,
                CurrentAuthor = Users.Where(q => q.Name == "student").First(),
                PromoterId = Users.Where(q => q.Name == "promoter").First().Id,
                Promoter = Users.Where(q => q.Name == "promoter").First()},

                new ApplicationForm { Id = "456-789-123",
                AuthorName = "Author2",
                AuthorAddress = "AuthorAddress2",
                AuthorZipCode = "663-400",
                AuthorCity = "Ostrów Wielkopolski",
                Title = "Title2",
                School = "School2",
                TypeOfEngineeringWork = "TypeOfEngineeringWork2",
                CurrentAuthorId = Users.Where(q => q.Name == "student").First().Id,
                CurrentAuthor = Users.Where(q => q.Name == "student").First(),
                PromoterId = Users.Where(q => q.Name == "promoter").First().Id,
                Promoter = Users.Where(q => q.Name == "promoter").First()},

                new ApplicationForm { Id = "789-123-456",
                AuthorName = "Author3",
                AuthorAddress = "AuthorAddress3",
                AuthorZipCode = "663-400",
                AuthorCity = "Ostrów Wielkopolski",
                Title = "Title3",
                School = "School3",
                TypeOfEngineeringWork = "TypeOfEngineeringWork3",
                CurrentAuthorId = Users.Where(q => q.Name == "student").First().Id,
                CurrentAuthor = Users.Where(q => q.Name == "student").First(),
                PromoterId = Users.Where(q => q.Name == "promoter").First().Id,
                Promoter = Users.Where(q => q.Name == "promoter").First()}
            }.AsQueryable();


            var mockSetForm = MockDbSet.GetMockDbSetAsync(AppForms);
            mockSetForm.Setup(m => m.Include(It.IsAny<String>())).Returns(mockSetForm.Object);

            var mockContext = new Mock<ApplicationDbContext>();
            mockContext.Setup(c => c.ApplicationForm).Returns(mockSetForm.Object);

            var service = new StudentController(mockContext.Object);

            //Act
            var applicationForm = service.Details(null).Result as HttpStatusCodeResult;
            var applicationForm2 = service.Details("123").Result as HttpStatusCodeResult;
            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            var currentUserId = User.Identity.GetUserId();
            if (currentUserId != null)
            {
                var roles = await UserManager.GetRolesAsync(currentUserId);
                var nameOfRole = roles.FirstOrDefault();



                if (nameOfRole == "Student")
                {
                    return RedirectToAction("Index", "Student");
                }
                else if (nameOfRole == "Promoter")
                {
                    return RedirectToAction("Index", "Promoter");
                }
                else if (nameOfRole == "Reviewer")
                {
                    return RedirectToAction("Index", "Reviewer");
                }
                else if (nameOfRole == "Admin")
                {
                    return RedirectToAction("Index", "User");
                }
            }
            return RedirectToAction("Login", "Account");
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}
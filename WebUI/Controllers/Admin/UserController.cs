﻿using Domain;
using Domain.Models;
using Microsoft.AspNet.Identity.Owin;
using NLog;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using WebUI.ViewModels;

namespace WebUI.Controllers.Admin
{
    public class UserController : Controller
    {
        private ApplicationDbContext _context;
        private ApplicationUserManager _userManager;
        private static Logger logger = LogManager.GetCurrentClassLogger();


        public UserController()
        {
        }

        public UserController(ApplicationUserManager userManager, ApplicationDbContext context)
        {
            UserManager = userManager;
            Context = context;
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public ApplicationDbContext Context
        {
            get
            {
                return _context ?? HttpContext.GetOwinContext().GetUserManager<ApplicationDbContext>();
            }
            private set
            {
                _context = value;
            }
        }

        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> Index(string email, string userRole)
        {

            List<UserViewModel> userList = new List<UserViewModel>();
            var allUsers = await Context.Users.Where(q => q.IsDeactivated == false).ToListAsync();
            var usersWithRoles = allUsers.Select(u => new UserViewModel { User = u, Role = String.Join(",", Context.Roles.Where(role => role.Users.Any(user => user.UserId == u.Id)).Select(r => r.Name)) }).AsQueryable();

            if (!string.IsNullOrEmpty(email))
            {
                usersWithRoles = usersWithRoles.Where(q => q.User.Email.Contains(email)).AsQueryable();
            }

            if (!string.IsNullOrEmpty(userRole))
            {
                usersWithRoles = usersWithRoles.Where(q => q.Role.Contains(userRole)).AsQueryable();
            }

            return View(usersWithRoles.AsEnumerable());
        }

        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> Details(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                logger.Warn("Details get: User id is empty");
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "User id is empty");
            }

            var user = await UserManager.FindByIdAsync(id);
            if (user == null)
            {
                logger.Warn("Details get: User was not found");
                return new HttpNotFoundResult("User was not found");
            }

            var role = await UserManager.GetRolesAsync(user.Id);
            if (role == null)
            {
                logger.Warn("Details get: Role was not found");
                return new HttpNotFoundResult("Role was not found");
            }

            return View(new UserViewModel() { User = user, Role = role.FirstOrDefault() });
        }

        [Authorize(Roles = "Admin")]
        public ActionResult Create()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            foreach (var role in Context.Roles)
            {
                list.Add(new SelectListItem() { Value = role.Name, Text = role.Name });
            }
            ViewBag.Roles = list;

            return View();
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser
                {
                    UserName = model.Email,
                    Email = model.Email,
                    Name = model.FirstName + " " + model.LastName,
                };

                var result = await UserManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    result = await UserManager.AddToRoleAsync(user.Id, model.RoleName);
                }
                return RedirectToAction("Index");
            }
            else
            {
                List<SelectListItem> list = new List<SelectListItem>();
                foreach (var role in Context.Roles)
                {
                    list.Add(new SelectListItem() { Value = role.Name, Text = role.Name });
                }
                ViewBag.Roles = list;
                return View();
            }
        }

        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> Edit(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                logger.Warn("Edit get: User id is empty");
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "User id is empty");
            }

            List<SelectListItem> list = new List<SelectListItem>();
            foreach (var role in Context.Roles)
            {
                list.Add(new SelectListItem() { Value = role.Name, Text = role.Name });
            }
            ViewBag.Roles = list;


            var user = await UserManager.FindByIdAsync(id);
            if (user == null)
            {
                logger.Warn("Edit get: User was not found");
                return new HttpNotFoundResult("User was not found");
            }

            var Role = await UserManager.GetRolesAsync(user.Id);
            if (Role == null)
            {
                logger.Warn("Edit get: Role was not found");
                return new HttpNotFoundResult("Role was not found");
            }

            return View(new UserViewModel() { User = user, Role = Role.First() });
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(UserViewModel model)
        {
            if (ModelState.IsValid)
            { 
                var userToEdit = await UserManager.FindByIdAsync(model.User.Id);
                if (userToEdit == null)
                {
                    logger.Warn("Edit post: User was not found");
                    return new HttpNotFoundResult("User was not found");
                }

                userToEdit.Email = model.User.Email;
                userToEdit.UserName = model.User.Email;
                userToEdit.Name = model.User.Name;

                var resoult2 = await UserManager.UpdateAsync(userToEdit);

                var listOfRoles = await Context.Roles.ToListAsync();
                foreach (var role in listOfRoles)
                {
                    //Check that user have this role 
                    var isInRole = await UserManager.IsInRoleAsync(model.User.Id, role.Name);
                    if (isInRole)
                        await UserManager.RemoveFromRoleAsync(model.User.Id, role.Name);
                }

                //Add user to role 
                var resoult = await UserManager.AddToRoleAsync(model.User.Id, model.Role);
                //!resoult.Succeeded

                return RedirectToAction("Index");
            }
            else
            {
                List<SelectListItem> list = new List<SelectListItem>();
                foreach (var role in Context.Roles)
                {
                    list.Add(new SelectListItem() { Value = role.Name, Text = role.Name });
                }
                ViewBag.Roles = list;


                var user = await UserManager.FindByIdAsync(model.User.Id);
                if (user == null)
                {
                    logger.Warn("Edit post: User was not found");
                    return new HttpNotFoundResult("User was not found");
                }

                var Role = await UserManager.GetRolesAsync(user.Id);
                if (Role == null)
                {
                    logger.Warn("Edit post: Role was not found");
                    return new HttpNotFoundResult("Role was not found");
                }

                return View(new UserViewModel() { User = user, Role = Role.First() });
            }
        }

        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> Delete(string Id)
        {
            if (string.IsNullOrEmpty(Id))
            {
                logger.Warn("Delete get: User id is empty");
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "User id is empty");
            }

            var userToDelete = await Context.Users.Where(q => q.Id == Id).FirstOrDefaultAsync();
            if (userToDelete == null)
            {
                logger.Warn("Delete get: User was not found");
                return new HttpNotFoundResult("User was not found");
            }

            return View(userToDelete);
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Delete(ApplicationUser model)
        {
            var userToDelete = await Context.Users.Where(q => q.Id == model.Id).FirstOrDefaultAsync();
            if (userToDelete == null)
            {
                logger.Warn("Delete post: User was not found");
                return new HttpNotFoundResult("User was not found");
            }

            userToDelete.IsDeactivated = true;

            try
            {
                await Context.SaveChangesAsync();
                logger.Info("Delete post: User correctly removed");
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Delete post: User exception");
            }
            
            return RedirectToAction("Index");
        }


    }
}
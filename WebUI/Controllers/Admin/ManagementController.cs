﻿using Domain;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using Domain.Models;
using WebUI.ViewModels;
using Domain.Enum;
using System.IO;
using NLog;
using System.Net;

namespace WebUI.Controllers.Admin
{
    public class ManagementController : Controller
    {
        private ApplicationDbContext _context;
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public ManagementController()
        {

        }

        public ManagementController(ApplicationDbContext context)
        {
            Context = context;
        }

        public ApplicationDbContext Context
        {
            get
            {
                return _context ?? HttpContext.GetOwinContext().GetUserManager<ApplicationDbContext>();
            }
            private set
            {
                _context = value;
            }
        }

        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> Index(string authorName = null, string promoterEmail = null)
        {
            var allApplicationForms = await Context.ApplicationForm.Include(s => s.CurrentAuthor).Include(s => s.Promoter).Include(s => s.PdfFile).Include(s => s.Reviewers).Include(s => s.Reviews).ToListAsync();

            if (!string.IsNullOrEmpty(authorName))
            {
                allApplicationForms = allApplicationForms.Where(q => q.AuthorName.Contains(authorName)).ToList();
            }

            if (!string.IsNullOrEmpty(promoterEmail))
            {
                allApplicationForms = allApplicationForms.Where(q => q.Promoter.Email.Contains(promoterEmail)).ToList();
            }
            return View(allApplicationForms.AsEnumerable());
        }

        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> Details(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                logger.Warn("Details get: Application form id is empty");
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Application form id is empty");
            }

            var applicationForm = await Context.ApplicationForm.Include(s => s.CurrentAuthor).Include(s => s.Promoter).Include(s => s.PdfFile).Include(s => s.Reviewers).Include(s => s.Reviews).Where(q => q.Id == id).FirstOrDefaultAsync();
            if (applicationForm == null)
            {
                logger.Warn("Details get: Application form was not found");
                return new HttpNotFoundResult("Application form was not found");
            }

            return View(applicationForm);
        }

        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> Delete(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                logger.Warn("Delete get: Application form id is empty");
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Application form id is empty");
            }

            var applicationForm = await Context.ApplicationForm.Include(s => s.CurrentAuthor).Include(s => s.Promoter).Include(s => s.PdfFile).Include(s => s.Reviewers).Include(s => s.Reviews).Where(q => q.Id == id).FirstOrDefaultAsync();
            if (applicationForm == null)
            {
                logger.Warn("Delete get: Application form was not found");
                return new HttpNotFoundResult("Application form was not found");
            }

            return View(applicationForm);
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Delete(ApplicationForm model)
        {
            var applicationForm = await Context.ApplicationForm.Include(s => s.CurrentAuthor).Include(s => s.Promoter).Include(s => s.PdfFile).Include(s => s.Reviewers).Include(s => s.Reviews).Where(q => q.Id == model.Id).FirstOrDefaultAsync();
            if (applicationForm == null)
            {
                logger.Warn("Delete post: Application form was not found");
                return new HttpNotFoundResult("Application form was not found");
            }

            try
            {
                Context.ApplicationForm.Remove(applicationForm);
                Context.SaveChanges();
                logger.Info("Delete post: Application form correctly removed");
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Delete post: Application form exception");
                return new HttpStatusCodeResult(500);
            }
            
            return RedirectToAction("Index", "Management");
        }

        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> Edit(string id)
        {
            var applicationForm = await Context.ApplicationForm.Include(s => s.CurrentAuthor).Include(s => s.Promoter).Include(s => s.PdfFile).Include(s => s.Reviewers).Include(s => s.Reviews).Where(q => q.Id == id).FirstOrDefaultAsync();
            if (applicationForm == null)
            {
                logger.Warn("Edit get: Application form was not found");
                return new HttpNotFoundResult("Application form was not found");
            }

            List<SelectListItem> listOfPromoters = new List<SelectListItem>();

            var role = await Context.Roles.Where(q => q.Name == "Promoter").FirstAsync();
            var listOfUsers = await Context.Users.Where(q => q.Roles.Any(w => w.RoleId.Contains(role.Id)) && q.IsDeactivated == false).ToListAsync();

            foreach (var user in listOfUsers)
            {
                listOfPromoters.Add(new SelectListItem() { Value = user.Id, Text = user.UserName });
            }

            ViewBag.Promoters = listOfPromoters;

            ApplicationFormViewModels viewModels = new ApplicationFormViewModels();
            viewModels.ApplicationForm = applicationForm;

            return View(viewModels);
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(ApplicationFormViewModels model)
        {
            if (ModelState.IsValid)
            {
                var applicationForm = await Context.ApplicationForm.Include(s => s.CurrentAuthor).Include(s => s.Promoter).Include(s => s.PdfFile).Include(s => s.Reviewers).Include(s => s.Reviews).Where(q => q.Id == model.ApplicationForm.Id).FirstOrDefaultAsync();
                if (applicationForm == null)
                {
                    logger.Warn("Edit post: Application form was not found");
                    return new HttpNotFoundResult("Application form was not found");
                }

                var pdfFile = await Context.PdfModel.Where(q => q.CurrentApplicationFormId == model.ApplicationForm.Id).ToListAsync();

                applicationForm.Title = model.ApplicationForm.Title;
                applicationForm.School = model.ApplicationForm.School;
                applicationForm.TypeOfEngineeringWork = model.ApplicationForm.TypeOfEngineeringWork;
                applicationForm.PromoterId = model.ApplicationForm.PromoterId;
                applicationForm.Promoter = await Context.Users.Where(q => q.Id == model.ApplicationForm.PromoterId).FirstOrDefaultAsync();

                //Application allways have Engineering Work
                if (model.EngineeringWork != null)
                {
                    var EngineeringWork = pdfFile.Where(q => q.KindOfEngineeringWorks == KindOfEngineeringWorks.EngineeringWork).FirstOrDefault();
                    //copy pdf to memory 
                    MemoryStream target = new MemoryStream();
                    model.EngineeringWork.InputStream.CopyTo(target);

                    EngineeringWork.Name = model.EngineeringWork.FileName;
                    EngineeringWork.ContentType = model.EngineeringWork.ContentType;
                    EngineeringWork.File = target.ToArray();
                    EngineeringWork.CurrentApplicationFormId = applicationForm.Id;
                    EngineeringWork.CurrentApplicationForm = applicationForm;
                    EngineeringWork.KindOfEngineeringWorks = KindOfEngineeringWorks.EngineeringWork;
                }

                if (model.SummaryPl != null)
                {
                    var SummaryPl = pdfFile.Where(q => q.KindOfEngineeringWorks == KindOfEngineeringWorks.SummaryPl).FirstOrDefault();

                    if (SummaryPl == null)
                    {
                        SummaryPl = new PdfModel();
                        SummaryPl.Id = Guid.NewGuid().ToString();
                        Context.PdfModel.Add(SummaryPl);
                    }
                    MemoryStream SummaryPlTarget = new MemoryStream();
                    model.SummaryPl.InputStream.CopyTo(SummaryPlTarget);

                    SummaryPl.Name = model.SummaryPl.FileName;
                    SummaryPl.ContentType = model.SummaryPl.ContentType;
                    SummaryPl.File = SummaryPlTarget.ToArray();
                    SummaryPl.CurrentApplicationFormId = applicationForm.Id;
                    SummaryPl.CurrentApplicationForm = applicationForm;
                    SummaryPl.KindOfEngineeringWorks = KindOfEngineeringWorks.SummaryPl;

                }

                if (model.SummaryEn != null)
                {
                    var SummaryEn = pdfFile.Where(q => q.KindOfEngineeringWorks == KindOfEngineeringWorks.SummaryPl).FirstOrDefault();

                    if (SummaryEn == null)
                    {
                        SummaryEn = new PdfModel();
                        SummaryEn.Id = Guid.NewGuid().ToString();
                        Context.PdfModel.Add(SummaryEn);
                    }

                    MemoryStream SummaryEnTarget = new MemoryStream();
                    model.SummaryEn.InputStream.CopyTo(SummaryEnTarget);

                    SummaryEn.Name = model.SummaryEn.FileName;
                    SummaryEn.ContentType = model.SummaryEn.ContentType;
                    SummaryEn.File = SummaryEnTarget.ToArray();
                    SummaryEn.CurrentApplicationFormId = applicationForm.Id;
                    SummaryEn.CurrentApplicationForm = applicationForm;
                    SummaryEn.KindOfEngineeringWorks = KindOfEngineeringWorks.SummaryEn;

                }

                try
                {
                    await Context.SaveChangesAsync();
                    logger.Info("Edit post: Application form correctly edited");
                }
                catch (Exception ex)
                {
                    logger.Error(ex, "Edit post: Application form exception");
                    return new HttpStatusCodeResult(500);
                }
                

                return RedirectToAction("Index");
            }
            else
            {
                var applicationForm = await Context.ApplicationForm.Include(s => s.CurrentAuthor).Include(s => s.Promoter).Include(s => s.PdfFile).Include(s => s.Reviewers).Include(s => s.Reviews).Where(q => q.Id == model.ApplicationForm.Id).FirstOrDefaultAsync();
                if (applicationForm == null)
                {
                    logger.Warn("Edit post: Application form was not found");
                    return new HttpNotFoundResult("Application form was not found");
                }

                List<SelectListItem> listOfPromoters = new List<SelectListItem>();

                var role = await Context.Roles.Where(q => q.Name == "Promoter").FirstAsync();
                var listOfUsers = await Context.Users.Where(q => q.Roles.Any(w => w.RoleId.Contains(role.Id))).ToListAsync();

                foreach (var user in listOfUsers)
                {
                    listOfPromoters.Add(new SelectListItem() { Value = user.Id, Text = user.UserName });
                }

                ViewBag.Promoters = listOfPromoters;

                ApplicationFormViewModels viewModels = new ApplicationFormViewModels();
                viewModels.ApplicationForm = applicationForm;

                return View(viewModels);
            }
        }

        [Authorize(Roles = "Admin")]
        public ActionResult ReturnFile(string applicationFormId, KindOfEngineeringWorks kindOfEngineeringWorks)
        {
            if (string.IsNullOrEmpty(applicationFormId))
            {
                logger.Warn("ReturnFile get: Application form id is empty");
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Application form id is empty");
            }

            var pdfFile = Context.PdfModel.Where(q => q.CurrentApplicationFormId == applicationFormId && q.KindOfEngineeringWorks == kindOfEngineeringWorks).FirstOrDefault();
            if (pdfFile == null)
            {
                logger.Warn("ReturnFile get: Pdf file was not found");
                return new HttpNotFoundResult("Pdf file was not found");
            }

            return File(pdfFile.File, pdfFile.ContentType, pdfFile.Name);
        }

        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> DisplayReview(string reviewsId)
        {
            if (string.IsNullOrEmpty(reviewsId))
            {
                logger.Warn("DisplayReview get: Review id is empty");
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Review id is empty");
            }

            var review = await Context.Review.Where(q => q.Id == reviewsId).FirstOrDefaultAsync();
            if (review == null)
            {
                logger.Warn("DisplayReview get: Review was not found");
                return new HttpNotFoundResult("Review was not found");
            }
            return View(review);
        }

        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> EditReview(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                logger.Warn("EditReview get: Review id is empty");
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Review id is empty");
            }

            List<SelectListItem> list = new List<SelectListItem>();
            for (int i = 0; i < 10; i++)
            {
                list.Add(new SelectListItem() { Value = i.ToString(), Text = i.ToString() });
            }
            ViewBag.Numbers = list;

            var review = await Context.Review.Where(q => q.Id == id).FirstOrDefaultAsync();
            if (review == null)
            {
                logger.Warn("EditReview get: Review was not found");
                return new HttpNotFoundResult("Review was not found");
            }

            return View(review);
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditReview(Review model)
        {
            var review = await Context.Review.Where(q => q.Id == model.Id).FirstOrDefaultAsync();
            if (review == null)
            {
                logger.Warn("EditReview post: Review was not found");
                return new HttpNotFoundResult("Review was not found");
            }

            review.SubjectOfWork = model.SubjectOfWork;
            review.UsefulnessForPractice = model.UsefulnessForPractice;
            review.OwnResearch = model.OwnResearch;
            review.Originality = model.Originality;
            review.UsedLiterature = model.UsedLiterature;
            review.LanguageAndFormOfWork = model.LanguageAndFormOfWork;

            try
            {
                await Context.SaveChangesAsync();
                logger.Info("EditReview post: Application form correctly edited");
            }
            catch (Exception ex)
            {
                logger.Error(ex, "EditReview post: Application form exception");
                return new HttpStatusCodeResult(500);
            }
            
            return RedirectToAction("Index");
        }

        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> ReviewDelete(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                logger.Warn("ReviewDelete get: Review id is empty");
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Review id is empty");
            }

            var review = await Context.Review.Where(q => q.Id == id).FirstOrDefaultAsync();
            if (review == null)
            {
                logger.Warn("ReviewDelete get: Review was not found");
                return new HttpNotFoundResult("Review was not found");
            }

            return View(review);
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ReviewDelete(Review model)
        {
            var review = await Context.Review.Where(q => q.Id == model.Id).FirstOrDefaultAsync();
            if (review == null)
            {
                logger.Warn("ReviewDelete post: Review was not found");
                return new HttpNotFoundResult("Review was not found");
            }

            try
            {
                Context.Review.Remove(review);
                await Context.SaveChangesAsync();
                logger.Info("ReviewDelete post: Review correctly removing");
            }
            catch (Exception ex)
            {
                logger.Error(ex, "ReviewDelete post: Review exception");
                return new HttpStatusCodeResult(500);
            }
            
            return RedirectToAction("Index");
        }

    }
}
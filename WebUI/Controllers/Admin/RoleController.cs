﻿using Domain.Models;
using Microsoft.AspNet.Identity.Owin;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace WebUI.Controllers.Admin
{
    public class RoleController : Controller
    {
        private ApplicationRoleManager _roleManager;
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public RoleController()
        {
        }

        public RoleController(ApplicationRoleManager roleManager)
        {
            RoleManager = roleManager;
        }

        public ApplicationRoleManager RoleManager
        {
            get
            {
                return _roleManager ?? HttpContext.GetOwinContext().Get<ApplicationRoleManager>();
            }
            private set
            {
                _roleManager = value;
            }
        }

        [Authorize(Roles = "Admin")]
        public ActionResult Index()
        {
            List<ApplicationRole> list = new List<ApplicationRole>();
            foreach (var item in RoleManager.Roles)
            {
                ApplicationRole role = new ApplicationRole();
                role.Id = item.Id;
                role.Name = item.Name;
                role.IsCreateByUser = item.IsCreateByUser;

                list.Add(role);
            }
            return View(list);
        }

        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> Details(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                logger.Warn("Details get: Role id is empty");
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Role id is empty");
            }

            var role = await RoleManager.FindByIdAsync(id);
            if (role == null)
            {
                logger.Warn("Details get: Role was not found");
                return new HttpNotFoundResult("Role was not found");
            }

            return View(role);
        }

        [Authorize(Roles = "Admin")]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(ApplicationRole model)
        {
            if (!string.IsNullOrEmpty(model.Name))
            {
                var role = new ApplicationRole() { Name = model.Name, IsCreateByUser = model.IsCreateByUser };
                await RoleManager.CreateAsync(role);
                return RedirectToAction("Index");
            }
            else
                return View();
        }

        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> Edit(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                logger.Warn("Edit get: Role id is empty");
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Role id is empty");
            }

            var role = await RoleManager.FindByIdAsync(id);
            if (role == null)
            {
                logger.Warn("Edit get: Role was not found");
                return new HttpNotFoundResult("Role was not found");
            }

            return View(role);
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(ApplicationRole model)
        {
            if (!string.IsNullOrEmpty(model.Name))
            {
                var roleToEdit = await RoleManager.FindByIdAsync(model.Id);
                if (roleToEdit == null)
                {
                    logger.Warn("Edit post: Role was not found");
                    return new HttpNotFoundResult("Role was not found");
                }

                roleToEdit.Name = model.Name;
                roleToEdit.IsCreateByUser = model.IsCreateByUser;

                try
                {
                    await RoleManager.UpdateAsync(roleToEdit);
                    logger.Info("Edit post: Role correctly edited");
                }
                catch (Exception ex)
                {
                    logger.Error(ex, "Edit post: Role exception");
                    return new HttpStatusCodeResult(500);
                }
                

                return RedirectToAction("Index");
            }
            else
            {
                var role = await RoleManager.FindByIdAsync(model.Id);
                if (role == null)
                {
                    logger.Warn("Edit post: Role was not found");
                    return new HttpNotFoundResult("Role was not found");
                }

                return View(role);
            }
        }

        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> Delete(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                logger.Warn("Delete get: Role id is empty");
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Role id is empty");
            }

            var role = await RoleManager.FindByIdAsync(id);
            if (role == null)
            {
                logger.Warn("Delete get: Role was not found");
                return new HttpNotFoundResult("Role was not found");
            }

            return View(role);
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Delete(ApplicationRole model)
        {
            var role = await RoleManager.FindByIdAsync(model.Id);
            if (role == null)
            {
                logger.Warn("Delete post: Role was not found");
                return new HttpNotFoundResult("Role was not found");
            }

            try
            {
                await RoleManager.DeleteAsync(role);
                logger.Info("Delete post: Role correctly removed");
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Delete post: Role exception");
                return new HttpStatusCodeResult(500);
            }
            

            return RedirectToAction("Index");
        }
    }
}
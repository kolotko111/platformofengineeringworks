﻿using Domain;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using System.Threading.Tasks;
using Domain.Models;
using WebUI.ViewModels;
using NLog;
using System.Net;

namespace WebUI.Controllers.Admin
{
    public class ReviewerAssignmentController : Controller
    {
        private ApplicationDbContext _context;
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public ReviewerAssignmentController()
        {
        }

        public ReviewerAssignmentController(ApplicationDbContext context)
        {
            Context = context;
        }


        public ApplicationDbContext Context
        {
            get
            {
                return _context ?? HttpContext.GetOwinContext().GetUserManager<ApplicationDbContext>();
            }
            private set
            {
                _context = value;
            }
        }

        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> Index()
        {
            var listOfApplicationForms =  await Context.ApplicationForm.Include(c => c.CurrentAuthor).Include(e => e.Promoter).Where(e => e.PromotorConfirmedForm == true).Where(r => r.Reviewers.Count == 0).ToListAsync();
            return View(listOfApplicationForms.AsEnumerable());
        }

        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> ReviewerAssignmen(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                logger.Warn("ReviewerAssignmen get: Application form id is empty");
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Application form id is empty");
            }

            List<SelectListItem> listOfReviewer = new List<SelectListItem>();

            var role = await Context.Roles.Where(q => q.Name == "Reviewer").FirstOrDefaultAsync();
            var listOfUsers = Context.Users.Where(q => q.Roles.Any(w => w.RoleId.Contains(role.Id)) && q.IsDeactivated == false).AsQueryable();

            foreach (var user in listOfUsers)
            {
                listOfReviewer.Add(new SelectListItem() { Value = user.Id, Text = user.UserName });
            }

            ViewBag.Reviewer = listOfReviewer;

            var formToAssignReviewer = await Context.ApplicationForm.Include(c => c.CurrentAuthor).Include(e => e.Promoter).Where(q => q.Id == id).FirstOrDefaultAsync();
            if (formToAssignReviewer == null)
            {
                logger.Warn("ReviewerAssignmen get: Application form was not found");
                return new HttpNotFoundResult("Application form was not found");
            }

            var model = new ReviewerAssignmentViewModels() {
                Id = formToAssignReviewer.Id,
                AuthorName = formToAssignReviewer.AuthorName,
                AuthorAddress = formToAssignReviewer.AuthorAddress,
                AuthorZipCode = formToAssignReviewer.AuthorZipCode,
                AuthorCity = formToAssignReviewer.AuthorCity,
                Title = formToAssignReviewer.Title,
                School = formToAssignReviewer.School,
                TypeOfEngineeringWork = formToAssignReviewer.TypeOfEngineeringWork,
                DateOfCreateForm = formToAssignReviewer.DateOfCreateForm
            };
            return View(model);
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ReviewerAssignmen(ReviewerAssignmentViewModels model)
        {
            if (ModelState.IsValid)
            {
                var firstReviewer = await Context.Users.Where(q => q.Id == model.FirstReviewer).FirstOrDefaultAsync();
                if (firstReviewer == null)
                {
                    logger.Warn("ReviewerAssignmen post: First reviewer was not found");
                    return new HttpNotFoundResult("First reviewer was not found");
                }

                var secondRreviewer = await Context.Users.Where(q => q.Id == model.SecondReviewer).FirstOrDefaultAsync();
                if (secondRreviewer == null)
                {
                    logger.Warn("ReviewerAssignmen post: Second reviewer was not found");
                    return new HttpNotFoundResult("Second reviewer was not found");
                }

                var form = await Context.ApplicationForm.Where(q => q.Id == model.Id).FirstOrDefaultAsync();
                if (form == null)
                {
                    logger.Warn("ReviewerAssignmen post: Application form was not found");
                    return new HttpNotFoundResult("Application form was not found");
                }

                FormUser firstReviewerAssignment = new FormUser();
                FormUser secondReviewerAssignment = new FormUser();

                firstReviewerAssignment.FormRefId = form.Id;
                firstReviewerAssignment.UserRefId = firstReviewer.Id;
                firstReviewerAssignment.ApplicationForm = form;
                firstReviewerAssignment.ApplicationUser = firstReviewer;


                secondReviewerAssignment.FormRefId = form.Id;
                secondReviewerAssignment.UserRefId = secondRreviewer.Id;
                secondReviewerAssignment.ApplicationForm = form;
                secondReviewerAssignment.ApplicationUser = secondRreviewer;

                try
                {
                    Context.FormUser.Add(firstReviewerAssignment);
                    Context.FormUser.Add(secondReviewerAssignment);
                    await Context.SaveChangesAsync();
                    logger.Info("ReviewerAssignmen post: Reviewer assignmented");
                }
                catch (Exception ex)
                {
                    logger.Error(ex, "ReviewerAssignmen post: ReviewerAssignmen exception");
                    return new HttpStatusCodeResult(500);
                }
                

                return RedirectToAction("Index");
            }
            else
            {
                List<SelectListItem> listOfReviewer = new List<SelectListItem>();

                var role = await Context.Roles.Where(q => q.Name == "Reviewer").FirstOrDefaultAsync();
                var listOfUsers = Context.Users.Where(q => q.Roles.Any(w => w.RoleId.Contains(role.Id))).AsQueryable();

                foreach (var user in listOfUsers)
                {
                    listOfReviewer.Add(new SelectListItem() { Value = user.Id, Text = user.UserName });
                }

                ViewBag.Reviewer = listOfReviewer;

                var formToAssignReviewer = await Context.ApplicationForm.Include(c => c.CurrentAuthor).Include(e => e.Promoter).Where(q => q.Id == model.Id).FirstOrDefaultAsync();
                if (formToAssignReviewer == null)
                {
                    logger.Warn("ReviewerAssignmen post: Application form was not found");
                    return new HttpNotFoundResult("Application form was not found");
                }

                var model2 = new ReviewerAssignmentViewModels() {
                    Id = formToAssignReviewer.Id,
                    AuthorName = formToAssignReviewer.AuthorName,
                    AuthorAddress = formToAssignReviewer.AuthorAddress,
                    AuthorZipCode = formToAssignReviewer.AuthorZipCode,
                    AuthorCity = formToAssignReviewer.AuthorCity,
                    Title = formToAssignReviewer.Title,
                    School = formToAssignReviewer.School,
                    TypeOfEngineeringWork = formToAssignReviewer.TypeOfEngineeringWork,
                    DateOfCreateForm = formToAssignReviewer.DateOfCreateForm
                };
                return View(model2);
            }
            
        }
    }
}
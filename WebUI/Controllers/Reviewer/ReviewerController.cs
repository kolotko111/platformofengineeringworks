﻿using Domain;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using Domain.Models;
using NLog;
using System.Net;

namespace WebUI.Controllers.Reviewer
{
    public class ReviewerController : Controller
    {
        private ApplicationDbContext _context;
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public ReviewerController()
        {
        }

        public ReviewerController( ApplicationDbContext context)
        {
            Context = context;
        }

        public ApplicationDbContext Context
        {
            get
            {
                return _context ?? HttpContext.GetOwinContext().GetUserManager<ApplicationDbContext>();
            }
            private set
            {
                _context = value;
            }
        }

        [Authorize(Roles = "Reviewer")]
        public ActionResult Index()
        {
            return View();
        }

        [Authorize(Roles = "Reviewer")]
        public async Task<ActionResult> ApplicationFormsForReview(string email = null, string titleOfEngineeringWork = null)
        {
            var currentUserId = User.Identity.GetUserId();
            if (string.IsNullOrEmpty(currentUserId))
            {
                logger.Warn("ApplicationFormsForReview get: Invalid current user id");
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Invalid current user id");
            }

            var FormsForReview = await Context.ApplicationForm.Where(q => q.Reviewers.Any(w => w.UserRefId == currentUserId) && !q.Reviews.Any(r => r.CurrentReviewerId == currentUserId)).ToListAsync();

            if (!string.IsNullOrEmpty(email))
            {
                FormsForReview = FormsForReview.Where(q => q.CurrentAuthor.Email.Contains(email)).ToList();
            }

            if (!string.IsNullOrEmpty(titleOfEngineeringWork))
            {
                FormsForReview = FormsForReview.Where(q => q.Title.Contains(titleOfEngineeringWork)).ToList();
            }
            
            return View(FormsForReview.AsEnumerable());
        }

        [Authorize(Roles = "Reviewer")]
        public ActionResult ApplicationFormsReviewed(string email = null, string titleOfEngineeringWork = null)
        {
            var currentUserId = User.Identity.GetUserId();
            if (string.IsNullOrEmpty(currentUserId))
            {
                logger.Warn("ApplicationFormsReviewed get: Invalid current user id");
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Invalid current user id");
            }

            var FormsReviewed = Context.ApplicationForm.Where(q => q.Reviewers.Any(w => w.UserRefId.Contains(currentUserId)) && q.Reviews.Any(r => r.CurrentReviewerId == currentUserId)).AsQueryable();

            if (!string.IsNullOrEmpty(email))
            {
                FormsReviewed = FormsReviewed.Where(q => q.CurrentAuthor.Email.Contains(email)).AsQueryable();
            }

            if (!string.IsNullOrEmpty(titleOfEngineeringWork))
            {
                FormsReviewed = FormsReviewed.Where(q => q.Title.Contains(titleOfEngineeringWork)).AsQueryable();
            }

            return View(FormsReviewed.AsEnumerable());
        }

        [Authorize(Roles = "Reviewer")]
        public async Task<ActionResult> Details(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                logger.Warn("Details get: Application form id is empty");
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Application form id is empty");
            }

            var formForReview = await Context.ApplicationForm.Include(c => c.CurrentAuthor).Include(e => e.Promoter).Include(e => e.PdfFile).Include(e => e.Reviews).Where(e => e.Id == id).FirstOrDefaultAsync();
            if (formForReview == null)
            {
                logger.Warn("Details get: Application form was not found");
                return new HttpNotFoundResult("Application form was not found");
            }

            return View(formForReview);
        }

        [Authorize(Roles = "Reviewer")]
        public async Task<ActionResult> ViewMore(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                logger.Warn("ViewMore get: Application form id is empty");
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Application form id is empty");
            }
            var viewMore = await Context.ApplicationForm.Include(c => c.CurrentAuthor).Include(e => e.Promoter).Include(e => e.PdfFile).Include(e => e.Reviewers).Where(e => e.Id == id).FirstOrDefaultAsync();
            if (viewMore == null)
            {
                logger.Warn("ViewMore get: Application form was not found");
                return new HttpNotFoundResult("Application form was not found");
            }

            return View(viewMore);
        }

        [Authorize(Roles = "Reviewer")]
        public async Task<ActionResult> CreateReview(string id)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            for (int i = 0; i < 10; i++)
            {
                list.Add(new SelectListItem() { Value = i.ToString(), Text = i.ToString() });
            }
            ViewBag.Numbers = list;

            var formForReview = await Context.ApplicationForm.Include(c => c.CurrentAuthor).Include(e => e.Promoter).Where(e => e.Id == id).FirstOrDefaultAsync();
            if (formForReview == null)
            {
                logger.Warn("CreateReview get: Application form was not found");
                return new HttpNotFoundResult("Application form was not found");
            }

            Review review = new Review();
            review.CurrentApplicationFormId = id;
            return View(review);
        }

        [HttpPost]
        [Authorize(Roles = "Reviewer")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> CreateReview(Review model)
        {
            if (ModelState.IsValid)
            {
                var currentUserId = User.Identity.GetUserId();
                if (string.IsNullOrEmpty(currentUserId))
                {
                    logger.Warn("CreateReview post: Invalid current user id");
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Invalid current user id");
                }

                Review review = new Review();
                review.Id = Guid.NewGuid().ToString();
                review.SubjectOfWork = model.SubjectOfWork;
                review.UsefulnessForPractice = model.UsefulnessForPractice;
                review.OwnResearch = model.OwnResearch;
                review.Originality = model.Originality;
                review.UsedLiterature = model.UsedLiterature;
                review.LanguageAndFormOfWork = model.LanguageAndFormOfWork;

                var formForReview = await Context.ApplicationForm.Include(c => c.CurrentAuthor).Include(e => e.Promoter).Where(e => e.Id == model.CurrentApplicationFormId).FirstOrDefaultAsync();
                if (formForReview == null)
                {
                    logger.Warn("Create post: Application form was not found");
                    return new HttpNotFoundResult("Application form was not found");
                }

                review.CurrentReviewerId = currentUserId;

                var currentUser = await Context.Users.Where(q => q.Id == currentUserId).FirstOrDefaultAsync();
                if (currentUser == null)
                {
                    logger.Warn("Create post: Current user was not found");
                    return new HttpNotFoundResult("Current user was not found");
                }
                review.CurrentReviewer = currentUser;
                review.CurrentApplicationFormId = model.CurrentApplicationFormId;
                review.CurrentApplicationForm = formForReview;

                try
                {
                    Context.Review.Add(review);
                    await Context.SaveChangesAsync();
                    logger.Info("Create post: Review correctly added");
                }
                catch (Exception ex)
                {
                    logger.Error(ex, "Create post: Review exception");
                }
                

                return RedirectToAction("Index");
            }
            else
            {
                List<SelectListItem> list = new List<SelectListItem>();
                for (int i = 0; i < 20; i++)
                {
                    list.Add(new SelectListItem() { Value = i.ToString(), Text = i.ToString() });
                }

                ViewBag.Numbers = list;

                var formForReview = await Context.ApplicationForm.Include(c => c.CurrentAuthor).Include(e => e.Promoter).Where(e => e.Id == model.CurrentApplicationFormId).FirstOrDefaultAsync();
                if (formForReview == null)
                {
                    logger.Warn("Create post: Application form was not found");
                    return new HttpNotFoundResult("Application form was not found");
                }

                Review review = new Review();
                review.CurrentApplicationFormId = model.CurrentApplicationFormId;
                return View(review);
            }
        }

        [Authorize(Roles = "Reviewer")]
        public async Task<ActionResult> DisplayReview(string reviewsId)
        {
            if (string.IsNullOrEmpty(reviewsId))
            {
                logger.Warn("DisplayReview get: Review id is empty");
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Review id is empty");
            }

            var review = await Context.Review.Where(q => q.Id == reviewsId).FirstOrDefaultAsync();
            if (review == null)
            {
                logger.Warn("DisplayReview get: Review was not found");
                return new HttpNotFoundResult("Review was not found");
            }

            return View(review);
        }
    }
}
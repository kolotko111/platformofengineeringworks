﻿using Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebUI.ViewModels
{
    public class UserViewModel
    {
        public ApplicationUser User { get; set; }
        public string Role { get; set; }
    }
}
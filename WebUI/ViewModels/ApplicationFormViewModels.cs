﻿using Domain.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Domain.Attribute;


namespace WebUI.ViewModels
{
    public class ApplicationFormViewModels
    {
        public ApplicationForm ApplicationForm { get; set; }

        [Display(Name = "Engineering Work Pdf")]
        [CustomFileExtensions("pdf", ErrorMessage = "Please upload pdf file")]
        public HttpPostedFileBase EngineeringWork { get; set; }

        [Display(Name = "Summary Pl Pdf")]
        [CustomFileExtensions("pdf", ErrorMessage = "Please upload pdf file")]
        public HttpPostedFileBase SummaryPl { get; set; }

        [Display(Name = "Summary En Pdf")]
        [CustomFileExtensions("pdf", ErrorMessage = "Please upload pdf file")]
        public HttpPostedFileBase SummaryEn { get; set; }
    }
}
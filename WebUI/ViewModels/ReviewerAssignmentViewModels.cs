﻿using Domain.Attribute;
using Domain.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebUI.ViewModels
{
    public class ReviewerAssignmentViewModels
    {
        public string Id { get; set; }

        [Display(Name = "Author name ")]
        public string AuthorName { get; set; }

        [Display(Name = "Author address ")]
        public string AuthorAddress { get; set; }

        [Display(Name = "Author zip code ")]
        public string AuthorZipCode { get; set; }

        [Display(Name = "Author city ")]
        public string AuthorCity { get; set; }

        [Display(Name = "The title of engineering work ")]
        public string Title { get; set; }

        [Display(Name = "Name of school")]
        public string School { get; set; }

        [Display(Name = "Type of engineering work")]
        public string TypeOfEngineeringWork { get; set; }

        [Display(Name = "Date of create form")]
        public DateTime DateOfCreateForm { get; set; }

        [Required(ErrorMessage = "Please select reviewer")]
        [Display(Name = "First reviewer ")]
        public String FirstReviewer { get; set; }

        [Required(ErrorMessage = "Please select reviewer")]
        [Display(Name = "Second reviewer ")]
        [UnlikeAttribute("FirstReviewer")]
        public String SecondReviewer { get; set; }

    }
}
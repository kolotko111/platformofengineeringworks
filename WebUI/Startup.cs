﻿using Domain;
using Domain.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(WebUI.Startup))]
namespace WebUI
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            CreateRolesAndUsers();
        }

        // In this method we create default User roles and Admin user 
        private void CreateRolesAndUsers()
        {
            ApplicationDbContext context = new ApplicationDbContext();

            var roleManager = new RoleManager<ApplicationRole>(new RoleStore<ApplicationRole>(context));
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));


            // In Startup iam creating first Admin Role and creating a default Admin User   
            if (!roleManager.RoleExists("Admin"))
            {

                // Create Admin rool   
                var role = new ApplicationRole();
                role.Name = "Admin";
                role.IsCreateByUser = false;
                roleManager.Create(role);
            }

            if (!roleManager.RoleExists("Student"))
            {

                // Create Student rool   
                var role = new ApplicationRole();
                role.Name = "Student";
                role.IsCreateByUser = true;
                roleManager.Create(role);
            }

            if (!roleManager.RoleExists("Promoter"))
            {
                // Create Promoter rool   
                var role = new ApplicationRole();
                role.Name = "Promoter";
                role.IsCreateByUser = true;
                roleManager.Create(role);
            }

            if (!roleManager.RoleExists("Reviewer"))
            {
                // Create Reviewer rool   
                var role = new ApplicationRole();
                role.Name = "Reviewer";
                role.IsCreateByUser = false;
                roleManager.Create(role);
            }

            //if admin does not exist, create admin
            if (userManager.FindByName("sanostrowtest@gmail.com") == null)
            {
                //Here we create a Admin super user who will maintain the website           
                var user = new ApplicationUser();
                user.UserName = "sanostrowtest@gmail.com";
                user.Name = "San Ostrów";
                user.Email = "sanostrowtest@gmail.com";

                string userPassword = "ZAQ!2wsx";
                var userCreated = userManager.Create(user, userPassword);

                //Add default User to Role Admin   
                if (userCreated.Succeeded)
                {
                    var result1 = userManager.AddToRole(user.Id, "Admin");
                }
            }

            //if student does not exist, create student
            if (userManager.FindByName("student@wp.pl") == null)
            {

                var user = new ApplicationUser();
                user.UserName = "student@wp.pl";
                user.Name = "student";
                user.Email = "student@wp.pl";

                string userPassword = "ZAQ!2wsx";
                var userCreated = userManager.Create(user, userPassword);

                //Add default User to Role Student   
                if (userCreated.Succeeded)
                {
                    var result1 = userManager.AddToRole(user.Id, "Student");
                }
            }
            //if promoter does not exist, create student
            if (userManager.FindByName("promoter@wp.pl") == null)
            {

                var user = new ApplicationUser();
                user.UserName = "promoter@wp.pl";
                user.Name = "promoter";
                user.Email = "promoter@wp.pl";

                string userPassword = "ZAQ!2wsx";
                var userCreated = userManager.Create(user, userPassword);

                //Add default User to Role promoter   
                if (userCreated.Succeeded)
                {
                    var result1 = userManager.AddToRole(user.Id, "Promoter");
                }
            }

            //if reviewer does not exist, create reviewer
            if (userManager.FindByName("reviewer@wp.pl") == null)
            {

                var user = new ApplicationUser();
                user.UserName = "reviewer@wp.pl";
                user.Name = "reviewer";
                user.Email = "reviewer@wp.pl";

                string userPassword = "ZAQ!2wsx";
                var userCreated = userManager.Create(user, userPassword);

                //Add default User to Role promoter   
                if (userCreated.Succeeded)
                {
                    var result1 = userManager.AddToRole(user.Id, "Reviewer");
                }
            }

            //if reviewer does not exist, create reviewer
            if (userManager.FindByName("reviewer2@wp.pl") == null)
            {

                var user = new ApplicationUser();
                user.UserName = "reviewer2@wp.pl";
                user.Name = "reviewer2";
                user.Email = "reviewer2@wp.pl";

                string userPassword = "ZAQ!2wsx";
                var userCreated = userManager.Create(user, userPassword);

                //Add default User to Role promoter   
                if (userCreated.Succeeded)
                {
                    var result1 = userManager.AddToRole(user.Id, "Reviewer");
                }
            }
        }
    }
}

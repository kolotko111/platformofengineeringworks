﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Models
{
    class ReviewConfiguration : EntityTypeConfiguration<Review>
    {
        public ReviewConfiguration()
        {
            this.HasRequired<ApplicationForm>(s => s.CurrentApplicationForm)
                .WithMany(g => g.Reviews)
                .HasForeignKey<string>(s => s.CurrentApplicationFormId);
                

            this.HasRequired<ApplicationUser>(s => s.CurrentReviewer)
                .WithMany(g => g.Reviews)
                .HasForeignKey<string>(s => s.CurrentReviewerId)
                .WillCascadeOnDelete(false);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Models
{
    public class Review
    {
        public string Id { get; set; }
        [Required(ErrorMessage = "Please provide the subject of work")]
        [Display(Name = "Subject of work")]
        [Range(0,9, ErrorMessage = "Please select a number between 0 and 9")]
        public int SubjectOfWork { get; set; }

        [Required(ErrorMessage = "Please provide the usefulness for practice")]
        [Display(Name = "Usefulness for practice")]
        [Range(0, 9, ErrorMessage = "Please select a number between 0 and 9")]
        public int UsefulnessForPractice { get; set; }

        [Required(ErrorMessage = "Please provide the own research")]
        [Display(Name = "Own research")]
        [Range(0, 9, ErrorMessage = "Please select a number between 0 and 9")]
        public int OwnResearch { get; set; }

        [Required(ErrorMessage = "Please provide the originality")]
        [Display(Name = "Originality")]
        [Range(0, 9, ErrorMessage = "Please select a number between 0 and 9")]
        public int Originality { get; set; }

        [Required(ErrorMessage = "Please provide the used literature")]
        [Display(Name = "Used literature")]
        [Range(0, 9, ErrorMessage = "Please select a number between 0 and 9")]
        public int UsedLiterature { get; set; }

        [Required(ErrorMessage = "Please provide the language and form of work")]
        [Display(Name = "Language and form of work")]
        [Range(0, 9, ErrorMessage = "Please select a number between 0 and 9")]
        public int LanguageAndFormOfWork { get; set; }

        public string CurrentApplicationFormId { get; set; }
        public ApplicationForm CurrentApplicationForm { get; set; }

        public string CurrentReviewerId { get; set; }
        public ApplicationUser CurrentReviewer { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Models
{
    public class FormUser
    {
        public string FormRefId { get; set; } 

        public string UserRefId { get; set; }

        public ApplicationForm ApplicationForm { get; set; }

        public ApplicationUser ApplicationUser { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Models
{
    class ApplicationFormConfiguration : EntityTypeConfiguration<ApplicationForm>
    {
        public ApplicationFormConfiguration()
        {
            //Relation One to Many betwean Author and Application Form
            this.HasRequired<ApplicationUser>(r => r.CurrentAuthor)
                .WithMany(t => t.ApplicationForm)
                .HasForeignKey<string>(y => y.CurrentAuthorId)
                .WillCascadeOnDelete(true);

            //Relation One to Many betwean Promoter and Application Form
            this.HasRequired<ApplicationUser>(s => s.Promoter)
                .WithMany(e => e.ApplicationFormForPromoter)
                .HasForeignKey<string>(s => s.PromoterId)
                .WillCascadeOnDelete(false);

            //Relation One to Many betwean PdfFile and Application Form
            this.HasMany<PdfModel>(r => r.PdfFile)
                .WithRequired(t => t.CurrentApplicationForm)
                .HasForeignKey<string>(y => y.CurrentApplicationFormId)
                .WillCascadeOnDelete(true);

            ////Relation Many to Many betwean Application User and Application Form
            //this.HasMany<ApplicationUser>(s => s.Reviewers)
            //    .WithMany(c => c.FormsForReview)
            //    .Map(cs =>
            //    {
            //        cs.MapLeftKey("FormRefId");
            //        cs.MapRightKey("UserRefId");
            //        cs.ToTable("FormUser");
            //    });
        }
    }
}

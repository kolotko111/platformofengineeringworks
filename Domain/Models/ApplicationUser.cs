﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit https://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        //public ApplicationUser()
        //{
        //    this.FormsForReview = new HashSet<ApplicationForm>();
        //}

        //First and last name of the user
        [Required]
        public string Name { get; set; }

        //Only for user with role Student
        public ICollection<ApplicationForm> ApplicationForm { get; set; }

        //Only for user with role Promoter 
        public ICollection<ApplicationForm> ApplicationFormForPromoter { get; set; }

        //Forms for reviewer
        public virtual ICollection<FormUser> FormsForReview { get; set; }

        //User is not deleted, is only deactivated
        public bool IsDeactivated { get; set; }

        //Reviews of form
        public virtual ICollection<Review> Reviews { get; set; }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Models
{
    class FormUserConfiguration : EntityTypeConfiguration<FormUser>
    {
        public FormUserConfiguration()
        {
            this.HasKey(i => new { i.FormRefId, i.UserRefId });

            this.HasRequired(i => i.ApplicationForm)
                .WithMany(i => i.Reviewers)
                .HasForeignKey(i => i.FormRefId)
                .WillCascadeOnDelete(false);

            this.HasRequired(i => i.ApplicationUser)
                .WithMany(i => i.FormsForReview)
                .HasForeignKey(i => i.UserRefId)
                .WillCascadeOnDelete(false);
        }
    }
}

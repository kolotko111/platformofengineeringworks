﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Models
{
    public class ApplicationForm
    {
        //public ApplicationForm()
        //{
        //    this.Reviewers = new HashSet<ApplicationUser>();
        //}

        public string Id { get; set; }

        //First and last name of the user
        [Required(ErrorMessage = "Please provide author name")]
        [Display(Name = "Author name ")]
        public string AuthorName { get; set; }

        //Address for correspondence
        [Required(ErrorMessage = "Please provide author address")]
        [Display(Name = "Author address ")]
        public string AuthorAddress { get; set; }

        [Required(ErrorMessage = "Please provide author zip code")]
        [Display(Name = "Author zip code ")]
        [RegularExpression(@"[0-9][0-9]-[0-9][0-9][0-9]", ErrorMessage = "Wrong author zip code")]
        [MaxLength(6)]
        public string AuthorZipCode { get; set; }

        [Required(ErrorMessage = "Please provide author city")]
        [Display(Name = "Author city ")]
        public string AuthorCity { get; set; }

        [Required(ErrorMessage = "Please provide the title of the engineering work")]
        [Display(Name = "The title of engineering work ")]
        public string Title { get; set; }

        [Required(ErrorMessage = "Please provide the name of school")]
        [Display(Name = "Name of school")]
        public string School { get; set; }

        [Required(ErrorMessage = "Please provide the type of engineering work")]
        [Display(Name = "Type of engineering work")]
        public string TypeOfEngineeringWork { get; set; }

        [Display(Name = "Promotor confirmed form")]
        public bool PromotorConfirmedForm { get; set; }

        [Display(Name = "Date of create form")]
        public DateTime DateOfCreateForm  { get; set; }

        //Author of ApplicationForm
        public string CurrentAuthorId { get; set; }
        public virtual ApplicationUser CurrentAuthor { get; set; }

        //Promoter for form 
        [Required(ErrorMessage = "Please select a promoter")]
        public string PromoterId { get; set; }
        public ApplicationUser Promoter { get; set; }

        //Pdf files 
        public ICollection<PdfModel> PdfFile { get; set; }

        //Reviewers for form
        public virtual ICollection<FormUser> Reviewers { get; set; }

        //Reviews of form
        public ICollection<Review> Reviews { get; set; }


    }
}

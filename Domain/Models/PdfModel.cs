﻿using Domain.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Models
{
    public class PdfModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string ContentType { get; set; }
        public byte[] File { get; set; }
        public KindOfEngineeringWorks KindOfEngineeringWorks { get; set; }

        //Engineering work, summary PL, summary EN
        public string CurrentApplicationFormId { get; set; }
        public virtual ApplicationForm CurrentApplicationForm { get; set; }
    }
}

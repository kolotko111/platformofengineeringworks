﻿using Domain.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //Without this line we cant migrating
            base.OnModelCreating(modelBuilder);

            modelBuilder.Configurations.Add(new ApplicationFormConfiguration());
            modelBuilder.Configurations.Add(new ReviewConfiguration());
            modelBuilder.Configurations.Add(new FormUserConfiguration());
        }

        public virtual DbSet<ApplicationForm> ApplicationForm { get; set; }
        public virtual DbSet<PdfModel> PdfModel { get; set; }
        public virtual DbSet<Review> Review { get; set; }
        public virtual DbSet<FormUser> FormUser { get; set; }

    }
}

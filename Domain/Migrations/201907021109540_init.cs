namespace Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ApplicationForms",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        AuthorName = c.String(nullable: false),
                        AuthorAddress = c.String(nullable: false),
                        AuthorZipCode = c.String(nullable: false, maxLength: 6),
                        AuthorCity = c.String(nullable: false),
                        Title = c.String(nullable: false),
                        School = c.String(nullable: false),
                        TypeOfEngineeringWork = c.String(nullable: false),
                        PromotorConfirmedForm = c.Boolean(nullable: false),
                        DateOfCreateForm = c.DateTime(nullable: false),
                        CurrentAuthorId = c.String(nullable: false, maxLength: 128),
                        PromoterId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentAuthorId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.PromoterId)
                .Index(t => t.CurrentAuthorId)
                .Index(t => t.PromoterId);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.FormUsers",
                c => new
                    {
                        FormRefId = c.String(nullable: false, maxLength: 128),
                        UserRefId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.FormRefId, t.UserRefId })
                .ForeignKey("dbo.ApplicationForms", t => t.FormRefId)
                .ForeignKey("dbo.AspNetUsers", t => t.UserRefId)
                .Index(t => t.FormRefId)
                .Index(t => t.UserRefId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.Reviews",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        SubjectOfWork = c.Int(nullable: false),
                        UsefulnessForPractice = c.Int(nullable: false),
                        OwnResearch = c.Int(nullable: false),
                        Originality = c.Int(nullable: false),
                        UsedLiterature = c.Int(nullable: false),
                        LanguageAndFormOfWork = c.Int(nullable: false),
                        CurrentApplicationFormId = c.String(nullable: false, maxLength: 128),
                        CurrentReviewerId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ApplicationForms", t => t.CurrentApplicationFormId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentReviewerId)
                .Index(t => t.CurrentApplicationFormId)
                .Index(t => t.CurrentReviewerId);
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.PdfModels",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(),
                        ContentType = c.String(),
                        File = c.Binary(),
                        KindOfEngineeringWorks = c.Int(nullable: false),
                        CurrentApplicationFormId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ApplicationForms", t => t.CurrentApplicationFormId, cascadeDelete: true)
                .Index(t => t.CurrentApplicationFormId);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                        IsCreateByUser = c.Boolean(),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.ApplicationForms", "PromoterId", "dbo.AspNetUsers");
            DropForeignKey("dbo.PdfModels", "CurrentApplicationFormId", "dbo.ApplicationForms");
            DropForeignKey("dbo.ApplicationForms", "CurrentAuthorId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Reviews", "CurrentReviewerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Reviews", "CurrentApplicationFormId", "dbo.ApplicationForms");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.FormUsers", "UserRefId", "dbo.AspNetUsers");
            DropForeignKey("dbo.FormUsers", "FormRefId", "dbo.ApplicationForms");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.PdfModels", new[] { "CurrentApplicationFormId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.Reviews", new[] { "CurrentReviewerId" });
            DropIndex("dbo.Reviews", new[] { "CurrentApplicationFormId" });
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.FormUsers", new[] { "UserRefId" });
            DropIndex("dbo.FormUsers", new[] { "FormRefId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.ApplicationForms", new[] { "PromoterId" });
            DropIndex("dbo.ApplicationForms", new[] { "CurrentAuthorId" });
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.PdfModels");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.Reviews");
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.FormUsers");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.ApplicationForms");
        }
    }
}

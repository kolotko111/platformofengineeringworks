namespace Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class deactivatingUser : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "IsDeactivated", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "IsDeactivated");
        }
    }
}
